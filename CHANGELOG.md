# Changelog

All notable changes to this project will be documented in this file. This project adheres to [Semantic Versioning](http://semver.org/spec/v1.0.0.html).

## [Unreleased]

### Added
- US288 User help
- US290 New Forecast Run Navigation Button
- US291 View/Manage past Forecasted Run Data
- US316 visualisation of Prediction data
- US341 Forecast Delete option
- US363 User Profile avatar icon and navigations
- US287 Account Settings
- US364 notification icon navigation
- US289 Activity completion Notification
- US301 submission of feedback by users
- US343 Forecast run details
- US318 Option to download predicted data
- US347 Forecast configuration details
- Added Privacy policy
- Added user guide
- Added installation guide

### Removed
- US278 User login
- US398 Option to view details for each project
- US393 Deleting a Datsource
- US384 Showing version histories of each forecast run.
- US306 Dataset configuration for Bigcommerce
- US361 Dataset Configuration Detail View
- US397 View/Manage Created Projects
- US381 Selecting existed dataset for Running a new forecast
- US383 Project Grouping according to product category.
- US377 Dataset overview
- US385 Storing uploaded dataset
- US302 Showing datasets requirements to user
- US360 Uploaded Dataset Preview
- US340 Uploaded dataset detail visualization
- US317 Option to compare predicted data with actual data
- US305 Dataset Configuration for CSV files
- US303 Upload CSV file option
- US342 Forecast run again option


### Changed
- Date format changes
### Fixed
- Forecast Horizon issue fixed.



