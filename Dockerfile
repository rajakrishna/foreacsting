FROM node:16 as build

WORKDIR /webapp
ADD ["package.json", "yarn.lock", "./"]

RUN yarn install

ADD [".", "."]
RUN yarn build


FROM nginx:stable-alpine

COPY --from=build ./webapp/build /usr/share/nginx/html
