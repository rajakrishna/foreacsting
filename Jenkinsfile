String label = "forecasting-app-${UUID.randomUUID().toString()}" as String

properties([gitLabConnection('gitlab')])

podTemplate(label: label, yaml: """
apiVersion: v1
kind: Pod
spec:
  containers:
  - name: node
    image: node:16
    command: ['cat']
    tty: true
    resources:
      requests:
        cpu: "1300m"
        memory: "2.5Gi"
  - name: docker
    image: docker:19
    command: ['cat']
    tty: true
    volumeMounts:
    - name: dockersock
      mountPath: /var/run/docker.sock
    - name: docker-credentials
      mountPath: /root/.docker
  - name: kubectl
    image: bitnami/kubectl:1.20
    command: ['cat']
    tty: true
    securityContext:
      runAsUser: 1000
    volumeMounts:
    - name: kubeconfig
      mountPath: /.kube
  volumes:
  - name: dockersock
    hostPath:
      path: /var/run/docker.sock
  - name: docker-credentials
    secret:
      secretName: gcr-aiops-docker-credentials
      items:
      - key: .dockerconfigjson
        path: config.json
  - name: kubeconfig
    secret:
      secretName: kube-config-new
""") {
  String buildType = 'feature'
  String gitCommit = ''
  try {
    gitlabBuilds(builds: ['checkout','install','test', 'yarn build', 'docker build', 'deploy dev', 'deploy stage']) {
      node(label) {
        stage('checkout') {
          gitlabCommitStatus('checkout') {
            def scmVars = checkout scm
            echo "scm vars: ${scmVars}"
            gitCommit = scmVars["GIT_COMMIT"]
            echo "gitCommit: ${gitCommit}"
            sh "ls -la"
            def sourceBranch
            try {
              sourceBranch = gitlabSourceBranch
            } catch (ignored) {
              sourceBranch = BRANCH_NAME
            }
            echo "sourceBranch is ${sourceBranch}"
            try {
              if (sourceBranch =~ /release/) {
                buildType = 'release'
              } else if (sourceBranch == 'develop') {
                buildType = 'snapshot'
              }
            } catch (ignored) {
              buildType = 'feature'
            }
            echo "Building ${buildType} from branch ${sourceBranch}"
          }
        }
          stage('install') {
          gitlabCommitStatus('install') {
            container('node') {
              String buildTag = 'dev'
              if (buildType == 'snapshot') {
                buildTag = 'stage'
              } else if (buildType == 'release') {
                buildTag = 'prod'
              }
              dir('webapp') {
                sh """
                yarn install
                node -v
                npm -v
                """
              }
            }
          }
        }
          stage('test') {
          gitlabCommitStatus('test') {
            container('node') {
              String buildTag = 'dev'
              if (buildType == 'snapshot') {
                buildTag = 'stage'
              } else if (buildType == 'release') {
                buildTag = 'prod'
              }
              dir('webapp') {
                sh """
                yarn test
                """
              }
            }
          }
        }
        stage('yarn build') {
          gitlabCommitStatus('yarn build') {
            container('node') {
              String buildTag = 'dev'
              if (buildType == 'snapshot') {
                buildTag = 'stage'
              } else if (buildType == 'release') {
                buildTag = 'prod'
              }
              dir('webapp') {
                sh """
                  CI=false yarn build
                """
                // CI=false yarn build:${buildTag}
                //dir('build') {
                //  sh "tar -cvzf forecasting-app-${gitCommit}.tar.gz *"
                //}
                //archiveArtifacts artifacts: 'build/*.tar.gz', onlyIfSuccessful: true
              }
            }
          }
        }
        stage('docker build') {
          gitlabCommitStatus('docker build') {
            container('docker') {
              String tag = 'latest'
              if (buildType == 'snapshot') {
                tag = 'snapshot'
              } else if (buildType == 'release') {
                def packageVersion = sh returnStdout: true, script: 'awk -F\'"\' \'/"version": ".+"/{ print $4; exit; }\' package.json'
                tag = packageVersion.trim()
              }
              sh """
              docker build -t gcr.io/aiops-224805/forecasting-app:${tag} -f Dockerfile.single .
              docker push gcr.io/aiops-224805/forecasting-app:${tag}
              """
            }
          }
        }
        stage('deploy dev') {
          gitlabCommitStatus('deploy dev') {
            if (buildType == 'feature') {
              container('kubectl') {
                string overlay = 'dev'
                sh """
                kubectl kustomize ./kubernetes/${overlay}
                kubectl apply -k ./kubernetes/${overlay} --context=pt-sandbox
                kubectl rollout restart deployment forecasting-app -n forecasting-solution --context=pt-sandbox
                kubectl rollout status deployment forecasting-app -n forecasting-solution --context=pt-sandbox
                """
              }
            }
          }
        }
        stage('deploy stage') {
          gitlabCommitStatus('deploy stage') {
            if (buildType == 'snapshot') {
              container('kubectl') {
                string overlay = 'stage'
                sh """
                kubectl kustomize ./kubernetes/${overlay}
                kubectl apply -k ./kubernetes/${overlay} --context=kai-stage-gcp
                kubectl rollout restart deployment forecasting-app -n forecasting-solution --context=kai-stage-gcp
                kubectl rollout status deployment forecasting-app -n forecasting-solution --context=kai-stage-gcp
                """
              }
            }
          }
        }
      }
    }
    currentBuild.result = 'SUCCESS'
  } catch (e) {
    currentBuild.result = 'FAILURE'
    echo "${e}"
  }
}
