
import React from 'react'

import { fireEvent, screen } from '@testing-library/react'
import { renderWithProviders } from './test.utils'
import App from './App'
// We're using our own custom render function and not RTL's render.

test('App component test-cases running', async () => {
  renderWithProviders(<App />)

})