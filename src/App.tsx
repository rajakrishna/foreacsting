import NavBar from "./components/NavBar";
import { BrowserRouter as Router, Route, Routes } from "react-router-dom";
import ForeCastingLIst from "./components/ForeCastingLIst";
import Notifications from "./components/Notifications";
import Help from "./components/Help";
import InpuDataTable from "./components/InpuDataTable";

import Details from "./components/details";
import NewForecast from "./components/newforecast";
import AppRedirect from "./components/AppRedirect";
import { useSelector } from "react-redux";
import { ApplicationState } from "./store";
import UserGuide from "./components/user-guide";
import InstallationGuide from "./components/installation-guide";
import PrivacyPolicy from "./components/privacy-policy";

function App() {
  const email = useSelector((state: ApplicationState) => state.auth.email);

  return (
    <Router basename="/forecasting-app/">
      <div>
        {!email && <AppRedirect />}

        <NavBar />
        <Routes>
          <Route path="/privacy-policy" element={<PrivacyPolicy />} />

          <Route path="/user-guide" element={<UserGuide />} />

          <Route path="/installation-guide" element={<InstallationGuide />} />

          <Route path="/forecasting-runs/:id" element={<ForeCastingLIst />} />

          <Route path="/details/:id" element={<Details />} />
          <Route path="/notifications" element={<Notifications />} />
          <Route path="/help" element={<Help />} />

          <Route path="/new-forecast" element={<NewForecast />} />

          <Route path="/" element={<ForeCastingLIst />} />

          <Route path="/inputdata/:id" element={<InpuDataTable />} />
        </Routes>
      </div>
    </Router>
  );
}

export default App;
