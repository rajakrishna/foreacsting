import axios from './base';

// attempts to login the specified user with the usename and the password
export function login(data:any) {

  return axios.request({
    baseURL: process.env.REACT_APP_AUTH_URL,
    url: '/auth',
    method: 'POST',
    data: data
  })
};

export function register(registerDetails:any) {
  return axios.request({
    baseURL: process.env.REACT_APP_REG_URL,
    url: '/reg',
    method: 'POST',
    data: registerDetails
  })
};
export function requestPasswordReset(data:any) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/account/reset-password/init',
    method: 'POST',
    data: data,
    headers: {
      "accept":"application/json",
      "Content-Type":"application/json"
    }
  })
};

export function comfirmPasswordReset(data:any) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/account/reset-password/finish',
    method: 'POST',
    data: data,
    headers: {
      "accept":"application/json",
      "Content-Type":"application/json"
    }
  })
};


// {
//   "accessToken": "string"
// }
export function requestEmailVerification(data:any) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/account/email-verification/init',
    method: 'POST',
    data: data,
    headers: {
      "accept":"application/json",
      "Content-Type":"application/json"
    }
  })
};

// {
//   "accessToken": "string",
//   "confirmationCode": "string"
// }
export function comfirmEmailVerification(data:any) {
  return axios.request({
    baseURL: process.env.REACT_APP_USER_URL,
    url: '/account/email-verification/finish',
    method: 'POST',
    data: data,
    headers: {
      "accept":"application/json",
      "Content-Type":"application/json"
    }
  })
};



