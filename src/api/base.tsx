import axios from "axios";

export const cancellationToken = axios.CancelToken;
// configure an axios instance
const instance = axios.create({
  headers: {
    "Content-Type": "application/json",
  },
});

//Auto Logout on Unauthoried(401) response
instance.interceptors.response.use(
  function (response) {
    return response;
  },
  function (error) {
    if (error.response && error.response.status === 401) {
     // localStorage.clear();
     // sessionStorage.clear();
     // window.location.reload();
    //  window.location.href = 'https://login.bigcommerce.com/login';
    }
    return Promise.reject(error);
  }
);

export default instance;
