import axios from "./base";

export function getProducts(id?: any) {
  const token = localStorage.getItem("token");

  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/bigcommerce/products/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    }
  });
}
