import axios from "./base";

export function getAllSources(id?:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/sources?project_id=${id}`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
  export function createSource(data:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/sources",
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }
  export function updateSource(data:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/sources",
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }
  export function deleteSource(id: any) {
    const token = localStorage.getItem("token");
    
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/sources/${id}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
     
      data: null,
    });
  }
  export function getSourceDetails(id: any) {
    const token = localStorage.getItem("token");
    
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/sources/${id}`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
     
      data: null,
    });
  }
  export function getSampleRecords(id: any) {
    const token = localStorage.getItem("token");
    
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/sources/${id}/sample-records`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
     
      data: null,
    });
  }
  export function testConnection(data:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/sources/test-connection",
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }
  export function getUserInfo() {
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: '/api/v1/userinfo',
      method: 'GET',
      headers: {
        "accept":"application/json",
        "Content-Type":"application/json"
      }
    })
  };