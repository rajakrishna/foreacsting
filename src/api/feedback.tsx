import axios from "./base";

export function saveFeedBack(data:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/feedback",
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }