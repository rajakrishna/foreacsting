import axios from "./base";

export function getAllModels(id:any) {
  console.log('base',process.env.REACT_APP_FORECAST_API_URL)
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/models?extends=True&project_id=${id}`,

  //  url: "/api/v1/models?extends=True",
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function getAllDataSources(ID: any) {
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/sources/${ID}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function UploadDataSet(data: any) {
  const token = localStorage.getItem("token");
  const formData = new FormData();
  formData.append("file", data.file);

  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: "/api/v1/sources/upload",
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
    params: {
      projectName: data.projectName,
      modelName: data.modelName,
      fileName: data.fileName,
    },
    data: formData,
  });
}
export function CreateDataset(data: any) {
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_AIQ_API_URL,
    url: "/api/datasets",
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data: data,
  });
}
export function CreateModel(data: any) {
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/models`,
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data: data,
  });
}
export function UpdateModel(data: any, id: any) {
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/models/${id}`,
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data: data,
  });
}
export function GetModelDetails(id: any,project_id?:any) {
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/models/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    }
  });
}
export function CreateDataSource(data: any) {
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: "/api/v1/sources",
    method: "POST",
    headers: {
      Authorization: `Bearer ${token}`,
    },
    data: data,
  });
}
export function getDatasetSampleRecords(ID: any) {
  const token = localStorage.getItem("token");

  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/sources/${ID}/sample-records`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function getForecastResults(ID: any) {
  const token = localStorage.getItem("token");

  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
   // models/ref_id/forecast-results
    url: `/api/v1/models/${ID}/forecast-results`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function getWorkflowDetails(id: any) {
  const token = localStorage.getItem("token");
  return axios.request({
    baseURL:process.env.REACT_APP_AIQ_API_URL,
    url: `/api/mlworkflow-runs/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
  });
}
export function UploadActualData(data: any, id: any) {
  const token = localStorage.getItem("token");
  const formData = new FormData();
  formData.append("file", data.file);

  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/forecast-results/${id}`,
    method: "PUT",
    headers: {
      Authorization: `Bearer ${token}`,
      "Content-Type": "multipart/form-data",
    },
    params: {
      projectName: data.projectName,
      modelName: data.modelName,
      fileName: data.fileName,
    },
    data: formData,
  });
}
export function DeleteModel(id: any,project_id:any) {
  const token = localStorage.getItem("token");
  
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/models/${id}`,
    method: "DELETE",
    headers: {
      Authorization: `Bearer ${token}`,
    },
   
    data: null,
  });
}
export function getModelVersions(id: any) {
  const token = localStorage.getItem("token");
  
  return axios.request({
    baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `/api/v1/model-versions/${id}`,
    method: "GET",
    headers: {
      Authorization: `Bearer ${token}`,
    },
   
    data: null,
  });
}
export function getBigCommerceProducts() {
  const token = localStorage.getItem("token");
  return axios.request({
  //  baseURL:process.env.REACT_APP_FORECAST_API_URL,
    url: `https://api.bigcommerce.com/stores/8u1ya5e0cx/v2/orders/count`,
    method: "GET",
    headers: {
      'X-Auth-Token': `qy7tc19tboxvfbxaditfwvdihfrw28l`,
    },
  });
}
