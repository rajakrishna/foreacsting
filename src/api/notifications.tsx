import axios from "./base";

export function getAllNotifications() {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/notifications",
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
  export function updateNotification(data:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/notifications",
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }