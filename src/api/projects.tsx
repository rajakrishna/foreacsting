import axios from "./base";
export function getProjects() {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/projects",
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }
  export function createProject(data:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: "/api/v1/projects",
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }
  export function updateProject(data:any,id:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/projects/${id}`,
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }
  export function deleteProject(id:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/projects/${id}`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });
  }
  export function getProjectDetails(id:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/projects/${id}`,
      method: "GET",
      headers: {
        Authorization: `Bearer ${token}`,
      }
    });
  }
  export function getAllUsers() {
    const token = localStorage.getItem("token");

    return axios.request({
      url: 'https://sandbox.predera.com/account-manager/users',
      method: 'GET',
      headers: {
        Authorization: `Bearer ${token}`,
      },
    })
  };
  export function saveProjectMembers(data:any,id:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/projects/${id}/members`,
      method: "POST",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }
  export function updateProjectMembers(data:any,id:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/projects/${id}/members`,
      method: "PUT",
      headers: {
        Authorization: `Bearer ${token}`,
      },
      data:data
    });
  }
  export function deleteProjectMembers(id:any) {
    const token = localStorage.getItem("token");
    return axios.request({
      baseURL:process.env.REACT_APP_FORECAST_API_URL,
      url: `/api/v1/projects/${id}/members`,
      method: "DELETE",
      headers: {
        Authorization: `Bearer ${token}`,
      },
    });
  }