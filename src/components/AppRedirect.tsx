import React from "react";

import { Navigate } from "react-router-dom";

import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { login } from "../store/auth/actions";
export interface IProps {
  login: Function;
  token: any;
  path: any;
}

class AppRedirect extends React.Component<IProps, any> {
  render() {
    const { path } = this.props;
    return this.props.path === "/forecasting-app/user-guide" ||
      path === "/forecasting-app/installation-guide" || path === "/forecasting-app/privacy-policy" ? (
      <div></div>
    ) : (
      <Navigate to="/" replace={true} />
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
    path: state.router.location.pathname,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      login,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(AppRedirect);
