import React from "react";

import { connect } from "react-redux";
import { AnyAction, bindActionCreators } from "redux";
import { ThunkDispatch } from "redux-thunk";

import {
  Alert,
  Autocomplete,
  Box,
  Button,
  Container,
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  FormLabel,
  Grid,
  IconButton,
  Snackbar,
  Stack,
  TextField,
  Tooltip,
} from "@mui/material";
import { Form, FormGroup, Modal, Small, Tabs } from "@bigcommerce/big-design";

import { DataGrid } from "@mui/x-data-grid";

import SyncIcon from "@mui/icons-material/Sync";
import GppMaybeIcon from "@mui/icons-material/GppMaybe";
import MoreHorizIcon from "@mui/icons-material/MoreHoriz";

import { Link as RouterLink } from "react-router-dom";

import {
  createModel,
  fetchForeCastingRuns,
} from "../store/foreCasting/actions";
import { fetchProjects } from "../store/projects/actions";

import { userInfo } from "../store/auth/actions";
import { searchProducts } from "../store/bigcommerce/actions";
import { getDatasources } from "../store/datasource/actions";
import { DeleteModel } from "../api";

export interface IProps {
  fetchForeCastingRuns: Function;
  project_id: any;
  runs: any;
  userInfo: Function;
  searchProducts: Function;
  fetchProjects: Function;
  getDatasources: Function;
  createModel: Function;
  datasourceId: any;
  token: any;
  products: [];
}
const items = [
  { ariaControls: "content1", id: "tab1", title: "Active" },
  { ariaControls: "content2", id: "tab2", title: "Inactive" },
];
class ForeCastingLIst extends React.Component<IProps, any> {
  state = {
    openSuccessSnakbar: false,
    isOpen: false,
    openDialogBox: false,
    is_shedule: true,
    skus: "",
    dup_sku: "",
    message: "",
    modelRefId: "",
    forecast_horizon: "D",
    activeTab: "tab1",
    forecast_period: 7,
    forecast_horizons: [
      { value: "D", content: "Daily" },
      { value: "W", content: "Weekly" },
      { value: "M", content: "Monthly" },
      { value: "Y", content: "Yearly" },
    ],
    products: [],
  };
  componentDidMount() {
    this.props.fetchProjects().then((res: any) => {
      if (res) {
        this.props.fetchForeCastingRuns(this.props.project_id);
        this.getDatasources();
      }
    });
  }
  refresh = () => {
    this.props.fetchForeCastingRuns(this.props.project_id);
    this.getDatasources();
  };
  getDatasources = () => {
    this.props.getDatasources(this.props.project_id).then((res: any) => {
      this.getProducts();
      this.activeRuns(this.props.runs);
      this.inActiveRuns(this.props.runs);
    });
  };
  getProducts = () => {
    this.props.searchProducts(this.props.datasourceId).then((res: any) => {
      if (res) {
        this.setState({ products: res });
      }
    });
  };
  render() {
    return (
      <div className="auth-wraper">
        {this.ForeCastingList()}
        {this.newForm()}
      </div>
    );
  }
  getProductName = (sku: any) => {
    let productName = "";
    this.state.products.forEach((item: any) => {
      if (item.sku === sku) {
        productName = item.name;
      }
    });
    return productName;
  };

  handleClose = () => {
    this.setState({
      openDialogBox: false,
      openProjectDialogBox: false,
      openSuccessSnakbar: false,
    });
  };
  handleDelete = () => {
    DeleteModel(this.state.modelRefId, this.props.project_id).then(
      (res: any) => {
        this.setState({
          openSuccessSnakbar: true,
          openDialogBox: false,
          message: "The forecast has been deleted",
        });
        this.refresh();
      }
    );
  };
  handleClickOpen = () => {
    this.setState({ openDialogBox: true });
  };
  handleClickOpen1 = () => {
    this.setState({ openProjectDialogBox: true });
  };
  handleChange = (e: any) => {
    this.setState({ skus: e.sku, dup_sku: e.sku });
  };
  setActiveTab = () => {
    if (this.state.activeTab === "tab1") {
      this.setState({ activeTab: "tab2" });
    } else {
      this.setState({ activeTab: "tab1" });
    }
  };
  open = () => {
    this.setState({ isOpen: true,forecast_horizon:'D' });
  };
  getName = () => {
    let now = new Date();
    let timestamp = now.getFullYear().toString();
    timestamp += (now.getMonth() ? "0" : "") + now.getMonth().toString();
    timestamp += (now.getDate() ? "0" : "") + now.getDate().toString();
    timestamp += (now.getHours() ? "0" : "") + now.getDate().toString();

    return "forecast-" + timestamp;
  };
  submitModel = () => {
    let data = {
      name: this.getName(),
      type: "Demand Forecast",
      desc: this.getName() + "description",
      data_source: this.props.datasourceId,
      project_id: this.props.project_id,
      is_scheduled: this.state.is_shedule,
      parameters: {
        input_columns: [
          { time_col: "date" },
          { sku_col: "sku" },
          { timestamp_format: "%Y-%m-%d" },
          { forecast_period: this.state.forecast_period },
          { forecast_horizon: this.state.forecast_horizon },
          { sku: this.state.skus },
        ],
        target_column: "sales",
      },
    };
    this.setState({ dup_sku: "" });
    this.props.createModel({ ...data }).then((res: any) => {
      if (res) {
        this.props.fetchForeCastingRuns(this.props.project_id);
        this.setState({
          ref_id: res.ref_id,
          openSuccessSnakbar: true,
          isOpen: false,
          message: "The forecast has been created",
        });
      }
    });
  };

  horizonChange = (e: any) => {
    if (e.value === "D") {
      this.setState({ forecast_horizon: e.value, forecast_period: 7 });
    } else if (e.value === "W") {
      this.setState({ forecast_horizon: e.value, forecast_period: 4 });
    } else if (e.value === "M") {
      this.setState({ forecast_horizon: e.value, forecast_period: 3 });
    } else if (e.value === "Y") {
      this.setState({ forecast_horizon: e.value, forecast_period: 1 });
    }
  };
  handleCheckboxChange = () => {
    if (this.state.is_shedule) {
      this.setState({ is_shedule: false });
    } else {
      this.setState({ is_shedule: true });
    }
  };
  newForm() {
    return (
      <>
        <Modal
          actions={[
            {
              text: "Cancel",
              variant: "subtle",
              onClick: () => {
                this.setState({ isOpen: false });
              },
            },
            {
              text: "Save",
              disabled: !this.state.dup_sku,
              onClick: () => {
                this.submitModel();
              },
            },
          ]}
          closeOnClickOutside={false}
          closeOnEscKey={true}
          header="Configuration Details"
          isOpen={this.state.isOpen}
          onClose={() => {
            this.setState({ isOpen: false });
          }}
        >
          <Form>
            <Small>Select Product</Small>

            {this.state.products && (
              <FormGroup>
                <Autocomplete
                  disablePortal
                  id="combo-box-demo"
                  options={this.state.products}
                  sx={{ width: 300 }}
                  getOptionLabel={(option) =>
                    option["name"] + " (" + option["sku"] + ")"
                  }
                  onChange={(event, newValue) => {
                    this.handleChange(newValue);
                  }}
                  renderInput={(params) => (
                    <TextField
                      {...params}
                      label="Select Product"
                      placeholder="Product"
                      sx={{ width: "420px" }}
                      size="small"
                    />
                  )}
                />
              </FormGroup>
            )}

            <Small>Forecast horizon</Small>

            <FormGroup>
              <Autocomplete
                disablePortal
                id="combo-box-demo"
                options={this.state.forecast_horizons}
                sx={{ width: 300 }}
                getOptionLabel={(option) => option.content}
                defaultValue={this.state.forecast_horizons[0]}
                onChange={(event, newValue) => {
                  this.horizonChange(newValue);
                }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Horizon"
                    placeholder="Horizon"
                    sx={{ width: "420px" }}
                    size="small"
                  />
                )}
              />
            </FormGroup>
          </Form>
        </Modal>
      </>
    );
  }
  ForeCastingList() {
    return (
      <Container
        maxWidth="xl"
        sx={{
          backgroundColor: "#F0F2F2",
          fontFamily: "sans-serif",
          color: "rgba(76, 78, 100, 0.87)",
        }}
      >
        <Box height="100vh">
          <Box
            height="90vh"
            sx={{
              backgroundColor: "#F7F7F9",
              borderRadius: "10px",
              boxShadow: "rgb(76 78 100 / 22%) 0px 2px 10px 0px",
              p: "20px",
              mt: "20px",
            }}
          >
            <Box sx={{ mt: "20px", ml: "20px" }}>
              <Grid container spacing={2}>
                <Grid xs={6}>
                  <h2> Sales Forecasts</h2>
                </Grid>
                <Grid xs={6}>
                  <Button
                    onClick={this.open}
                    sx={{ float: "right", textTransform: "none", mt: "30px" }}
                    variant="contained"
                    size="small"
                  >
                    Create New Forecast
                  </Button>
                  <IconButton
                    title="Refresh"
                    onClick={this.refresh}
                    sx={{
                      float: "right",
                      textTransform: "none",
                      mr: "10px",
                      mt: "25px",
                    }}
                    aria-label="delete"
                  >
                    <SyncIcon />
                  </IconButton>
                </Grid>
              </Grid>
            </Box>
            <div>
              <Dialog
                open={this.state.openDialogBox}
                onClose={this.handleClose}
                aria-labelledby="alert-dialog-title"
                aria-describedby="alert-dialog-description"
              >
                <DialogTitle id="alert-dialog-title">
                  {"Are you sure you want to delete?"}
                </DialogTitle>
                <DialogContent>
                  <DialogContentText id="alert-dialog-description">
                    This operation cannot be Undone. The Forecast run will be
                    permanently deleted
                  </DialogContentText>
                </DialogContent>
                <DialogActions>
                  <Button onClick={this.handleClose}>Cancel</Button>
                  <Button color="error" onClick={this.handleDelete} autoFocus>
                    Delete
                  </Button>
                </DialogActions>
              </Dialog>
            </div>
            <div>
              <Snackbar
                open={this.state.openSuccessSnakbar}
                autoHideDuration={6000}
                onClose={this.handleClose}
                anchorOrigin={{
                  vertical: "bottom",
                  horizontal: "right",
                }}
              >
                <Alert
                  onClose={this.handleClose}
                  severity="success"
                  sx={{ width: "100%" }}
                >
                  {this.state.message}
                </Alert>
              </Snackbar>
              {this.props.runs ? (
                <>
                  <Tabs
                    activeTab={this.state.activeTab}
                    aria-label="Tab Content"
                    id="tab-example"
                    items={items}
                    onTabClick={this.setActiveTab}
                  />
                  <Box marginTop="large">
                    {this.state.activeTab === "tab1" && (
                      <div>{this.activeRuns(this.props.runs)}</div>
                    )}
                    {this.state.activeTab === "tab2" && (
                      <div>{this.inActiveRuns(this.props.runs)}</div>
                    )}
                  </Box>
                </>
              ) : (
                <span></span>
              )}
            </div>
          </Box>
        </Box>
      </Container>
    );
  }
  inActiveRuns(models: any) {
    const columns: any[] = [
      {
        field: "created_at",
        headerName: "Created",
        width: 230,
        type: "dateTime",
        headerClassName: "super-app-theme--header",
      },
      {
        field: "name",
        headerName: "Product",
        width: 230,
        headerClassName: "super-app-theme--header",
      },
      {
        field: "sku",
        headerName: "SKU",
        width: 200,
        headerClassName: "super-app-theme--header",
        renderCell: (cellValues: any) => {
          let skuIds = cellValues.value;
          skuIds = skuIds.join(",");
          return (
            <div>
              <span style={{ marginBottom: "10px" }}>
                {cellValues.value[0]}
              </span>
              {cellValues.value[1] && (
                <Tooltip title={skuIds} arrow>
                  <MoreHorizIcon
                    sx={{ cursor: "pointer", pt: "10px" }}
                    color="action"
                  />
                </Tooltip>
              )}
            </div>
          );
        },
      },

      {
        field: "forecast_horizon",
        headerName: "Horizon",
        width: 200,
        headerClassName: "super-app-theme--header",
        renderCell: (cellValues: any) => {
          if (cellValues.value === "D") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Daily
              </FormLabel>
            );
          } else if (cellValues.value === "W") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Weekly
              </FormLabel>
            );
          } else if (cellValues.value === "M") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Monthly
              </FormLabel>
            );
          } else {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Yearly
              </FormLabel>
            );
          }
        },
      },
      {
        field: "status",
        headerName: "Status",
        headerClassName: "super-app-theme--header",
        width: 130,

        renderCell: (cellValues: any) => {
          if (cellValues.value === "ERROR") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                  color: "red",
                }}
              >
                ERROR
              </FormLabel>
            );
          } else if (cellValues.value === "SUCCESS") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                  color: "green",
                }}
              >
                SUCCESS
              </FormLabel>
            );
          } else if (cellValues.value === "CREATED") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                CREATED
              </FormLabel>
            );
          } else {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                  color: "orange",
                }}
                color="info"
              >
                RUNNING
              </FormLabel>
            );
          }
        },
      },
      {
        field: "action",
        headerName: "Action",
        sortable: false,
        headerClassName: "super-app-theme--header",
        flex: 1,
        renderCell: (params: any) => {
          return (
            <Box>
              <Button
                component={RouterLink}
                to={`/details/${params.row.ref_id}`}
                aria-label="view"
                style={{ textTransform: "none", padding: "0px 0px" }}
                size="small"
                variant="outlined"
              >
                View
              </Button>
            </Box>
          );
        },
      },
    ];
    let modelList: any[] = [];
    let obj = {};
    models.forEach((item: any, index: number) => {
      if (!item.is_active) {
        const date = new Date(item.created_at + "Z");
        let skuId = "";
        let sku: any[] = [];
        let forecast_horizon = "";
        item.versions[0].parameters.input_columns.forEach((item2: any) => {
          if (item2.sku !== undefined) {
            skuId = item2.sku;
            sku = skuId.split(",");
          }
          if (item2.forecast_horizon !== undefined) {
            forecast_horizon = item2.forecast_horizon;
          }
        });

        obj = {
          ref_id: item.ref_id,
          name: this.getProductName(sku[0]),
          status: item.versions[item.versions.length - 1].status,
          id: index + 1,
          created_at: date.toLocaleString(),
          sku: skuId.split(","),
          forecast_horizon: forecast_horizon,
          // mape: " ",
        };
        modelList.push(obj);
      }
    });

    return (
      <div style={{ height: 400, marginTop: "20px", width: "100%" }}>
        {modelList ? (
          <DataGrid
            sx={{
              ".MuiDataGrid-columnSeparator": {
                display: "none",
              },
            }}
            components={{
              NoRowsOverlay: () => (
                <Stack
                  height="100%"
                  alignItems="center"
                  justifyContent="center"
                >
                  <GppMaybeIcon color="action" fontSize="large" />
                  No Data Found
                </Stack>
              ),
            }}
            rows={modelList}
            columns={columns}
            hideFooter={true}
          />
        ) : (
          <span></span>
        )}
      </div>
    );
  }
  activeRuns(models: any) {
    const columns: any[] = [
      {
        field: "created_at",
        headerName: "Created",
        width: 230,
        type: "dateTime",
        headerClassName: "super-app-theme--header",
      },
      {
        field: "name",
        headerName: "Product",
        width: 230,
        headerClassName: "super-app-theme--header",
      },
      {
        field: "sku",
        headerName: "SKU",
        width: 200,
        headerClassName: "super-app-theme--header",
        renderCell: (cellValues: any) => {
          let skuIds = cellValues.value;
          skuIds = skuIds.join(",");
          return (
            <div>
              <span style={{ marginBottom: "10px" }}>
                {cellValues.value[0]}
              </span>
              {cellValues.value[1] && (
                <Tooltip title={skuIds} arrow>
                  <MoreHorizIcon
                    sx={{ cursor: "pointer", pt: "10px" }}
                    color="action"
                  />
                </Tooltip>
              )}
            </div>
          );
        },
      },

      {
        field: "forecast_horizon",
        headerName: "Horizon",
        width: 200,
        headerClassName: "super-app-theme--header",
        renderCell: (cellValues: any) => {
          if (cellValues.value === "D") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Daily
              </FormLabel>
            );
          } else if (cellValues.value === "W") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Weekly
              </FormLabel>
            );
          } else if (cellValues.value === "M") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Monthly
              </FormLabel>
            );
          } else {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                Yearly
              </FormLabel>
            );
          }
        },
      },
      {
        field: "status",
        headerName: "Status",
        headerClassName: "super-app-theme--header",
        width: 130,

        renderCell: (cellValues: any) => {
          if (cellValues.value === "ERROR") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                  color: "red",
                }}
              >
                ERROR
              </FormLabel>
            );
          } else if (cellValues.value === "SUCCESS") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                  color: "green",
                }}
              >
                SUCCESS
              </FormLabel>
            );
          } else if (cellValues.value === "CREATED") {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                }}
              >
                CREATED
              </FormLabel>
            );
          } else {
            return (
              <FormLabel
                sx={{
                  fontSize: "0.875rem",
                  fontWeight: 500,
                  fontFamily: '"Roboto","Helvetica","Arial",sans-serif',
                  color: "orange",
                }}
                color="info"
              >
                RUNNING
              </FormLabel>
            );
          }
        },
      },
      {
        field: "action",
        headerName: "Action",
        sortable: false,
        headerClassName: "super-app-theme--header",
        flex: 1,
        renderCell: (params: any) => {
          return (
            <Box>
              <Button
                component={RouterLink}
                to={`/details/${params.row.ref_id}`}
                aria-label="view"
                style={{ textTransform: "none", padding: "0px 0px" }}
                size="small"
                variant="outlined"
              >
                View
              </Button>

              <Button
                variant="outlined"
                color="error"
                aria-label="view"
                onClick={() => {
                  this.setState({
                    openDialogBox: true,
                    modelRefId: params.row.ref_id,
                  });
                }}
                style={{
                  textTransform: "none",
                  padding: "0px 5px 0px 5px",
                  marginLeft: "10px",
                }}
                size="small"
              >
                Delete
              </Button>
            </Box>
          );
        },
      },
    ];
    let modelList: any[] = [];
    let obj = {};
    models.forEach((item: any, index: number) => {
      if (item.is_active) {
        const date = new Date(item.created_at + "Z");
        let skuId = "";
        let sku: any[] = [];
        let forecast_horizon = "";
        item.versions[0].parameters.input_columns.forEach((item2: any) => {
          if (item2.sku !== undefined) {
            skuId = item2.sku;
            sku = skuId.split(",");
          }
          if (item2.forecast_horizon !== undefined) {
            forecast_horizon = item2.forecast_horizon;
          }
        });

        obj = {
          ref_id: item.ref_id,
          name: this.getProductName(sku[0]),
          status: item.versions[item.versions.length - 1].status,
          id: index + 1,
          created_at: date.toLocaleString(),
          sku: skuId.split(","),
          forecast_horizon: forecast_horizon,
        };
        modelList.push(obj);
      }
    });

    return (
      <div style={{ height: 400, marginTop: "20px", width: "100%" }}>
        {modelList ? (
          <DataGrid
            sx={{
              ".MuiDataGrid-columnSeparator": {
                display: "none",
              },
            }}
            components={{
              NoRowsOverlay: () => (
                <Stack
                  height="100%"
                  alignItems="center"
                  justifyContent="center"
                >
                  <GppMaybeIcon color="action" fontSize="large" />
                  No Data Found
                </Stack>
              ),
            }}
            rows={modelList}
            columns={columns}
            hideFooter={true}
          />
        ) : (
          <span></span>
        )}
      </div>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
    runs: state.foreCasting.runs,
    users: state.projects.users,
    project_id: state.projects.project_id,
    projects: state.projects.projects,
    products: state.bigcommerce.products,
    datasourceId: state.datasource.datasourceId,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      fetchForeCastingRuns,
      userInfo,
      searchProducts,
      fetchProjects,
      getDatasources,
      createModel,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(ForeCastingLIst);
