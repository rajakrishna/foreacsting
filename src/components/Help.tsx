import * as React from "react";

import {
  Box,
  Button,
  Divider,
  TextField,
  Container,
  Grid,
  Breadcrumbs,
  Link,
  Typography,
} from "@mui/material";

import { Link as RouterLink } from "react-router-dom";

import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { submitFeedBack } from "../store/feedback/actions";
import HomeIcon from "@mui/icons-material/Home";

export interface IProps {
  submitFeedBack: Function;
}
class Help extends React.Component<IProps, any> {
  state = {
    feedback: "",
  };
  render() {
    return (
      <Container
        maxWidth="xl"
        sx={{
          backgroundColor: "#F0F2F2",
          fontFamily: "sans-serif",
          color: "rgba(76, 78, 100, 0.87)",
        }}
      >
        <Box sx={{ m: "20px", ml: "20px" }}>
          <Grid container spacing={2}>
            <Grid xs={6}>
              <h2>
                {" "}
                <div role="presentation">
                  <Breadcrumbs aria-label="breadcrumb">
                    <Link
                      underline="hover"
                      sx={{ display: "flex", alignItems: "center" }}
                      color="inherit"
                      component={RouterLink}
                      to={`/`}
                    >
                      <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
                      Home
                    </Link>

                    <Typography
                      sx={{ display: "flex", alignItems: "center" }}
                      color="text.primary"
                    >
                      Help{" "}
                    </Typography>
                  </Breadcrumbs>
                </div>
              </h2>
            </Grid>
          </Grid>
        </Box>
        <Box height="100vh">
          <Box
            sx={{
              backgroundColor: "#F7F7F9",
              p: "20px",
              borderRadius: "10px",
              boxShadow: "rgb(76 78 100 / 22%) 0px 2px 10px 0px",
            }}
          >
            <Box sx={{ mt: "20px", ml: "20px" }}>
              <Grid container spacing={2}>
                <Grid xs={8}>
                  <h2>Support</h2>
                </Grid>
              </Grid>
            </Box>
            <Box sx={{ m: "30px" }} className="auth-wraper">
              {this.Help()}
            </Box>
          </Box>
        </Box>
      </Container>
    );
  }
  Help() {
    return (
      <Box sx={{ m: "20px",mb:'20px' }}>
        <Box sx={{ width: "100%", m: "20px" }}>
          <div>
            <strong>Contact Us :</strong>
          </div>
          <div>
            <small>Email Id : support@predera.com</small>
          </div>
          <div>
            <small>
              Web site : <a href="https://predera.com/">www.predera.com</a>{" "}
            </small>
          </div>
        </Box>
        <Divider />
        <Box sx={{ m: "20px" }}>
          <div>
            <strong>Provide us your feedback</strong>
          </div>
          <div>
            <TextField
              sx={{ width: "100%", mt: "50px" }}
              id="outlined-textarea"
              label="Feedback"
              name="feedback"
              placeholder="Please Give Your Feedback"
              rows={6}
              value={this.state.feedback}
              onChange={this.changeInput}
              multiline
            />
          </div>
          <Button
            sx={{
              mt: "20px",
              float: "right",
              textTransform: "none",
            }}
            variant="outlined"
            size="small"
            onClick={this.submit}
            component={RouterLink}
            to={`/`}
          >
            Submit Feedback
          </Button>
          <br />
        </Box>
      </Box>
    );
  }
  changeInput = (e: any) => {
    this.setState({ feedback: e.target.value });
  };
  submit = () => {
    let obj = {
      feedback: this.state.feedback,
    };
    this.props.submitFeedBack(obj);
  };
}
const mapStateToProps = (state: any) => {
  return {
    notifications: state.notifications.notifications,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      submitFeedBack,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Help);
