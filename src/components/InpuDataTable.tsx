import * as React from "react";
import { DataGrid } from "@mui/x-data-grid";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { fetchForeCastingRuns } from "../store/foreCasting/actions";
import { getAllDataSources, getDatasetSampleRecords } from "../api";
import {
  Box,
  Breadcrumbs,
  Button,
  Container,
  Grid,
  Link,
  Typography,
} from "@mui/material";
import { Link as RouterLink } from "react-router-dom";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import HomeIcon from "@mui/icons-material/Home";

export interface IProps {
  fetchForeCastingRuns: Function;
  runs: any;
  refId: any;
  project_id: any;
}

class InpuDataTable extends React.Component<IProps, any> {
  state = {
    rows: [],
    columns: [],
    ref_id: "",
  };

  componentDidMount() {
    this.props.fetchForeCastingRuns(this.props.project_id);
    let id = window.location.href.substring(
      window.location.href.lastIndexOf("/") + 1
    );
    this.setState({ ref_id: id });
    this.getDataSources(id);
  }
  getDataSources = (id: any) => {
    this.props.runs.forEach((item: any) => {
      if (item.ref_id === id) {
        getAllDataSources(item.data_source).then((res) => {
          this.getSampleRecords(res.data.ref_id);
        });
      }
    });
  };
  getSampleRecords = (id: any) => {
    getDatasetSampleRecords(id).then((res) => {
      const data = res.data;
      let keys: any[] = [];
      keys = data[0];
      let list: any[] = [];
      let columnslist: any[] = [];
      keys.forEach((item) => {
        let obj = { field: item, headerName: item, width: 180 };
        columnslist.push(obj);
      });
      this.setState({ rows: keys, columns: columnslist });
      data.forEach((item: any, index: number) => {
        if (index !== 0) {
          let obj: any = {
            id: index + 1,
          };

          keys.forEach((item1) => {
            obj[item1] = item[keys.indexOf(item1)];
          });
          list.push(obj);
        }
      });

      this.setState({
        rows: list,
      });
    });
  };
  Breadcrumbs() {
    return (
      <div role="presentation">
        <Breadcrumbs aria-label="breadcrumb">
          <Link
            underline="hover"
            sx={{ display: "flex", alignItems: "center" }}
            color="inherit"
            component={RouterLink}
            to={`/projects`}
          >
            <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
            Home
          </Link>
          <Link
            underline="hover"
            sx={{ display: "flex", alignItems: "center" }}
            color="inherit"
            component={RouterLink}
            to={`/projects`}
          >
            Projects
          </Link>
          <Link
            underline="hover"
            sx={{ display: "flex", alignItems: "center" }}
            color="inherit"
            component={RouterLink}
            to={`/forecasting-runs/${this.props.project_id}`}
          >
            Sales Forecasts
          </Link>
          <Link
            underline="hover"
            sx={{ display: "flex", alignItems: "center" }}
            color="inherit"
            component={RouterLink}
            to={`/details/${this.state.ref_id}`}
          >
            Sales forecast Details
          </Link>
          <Typography
            sx={{ display: "flex", alignItems: "center" }}
            color="text.primary"
          >
            Input Data{" "}
          </Typography>
        </Breadcrumbs>
      </div>
    );
  }
  render() {
    return (
      <Container
        maxWidth="xl"
        sx={{
          backgroundColor: "#F0F2F2",
          fontFamily: "sans-serif",
          color: "rgba(76, 78, 100, 0.87)",
        }}
      >
        <Box sx={{ m: "20px", ml: "20px" }}>
          <Grid container spacing={2}>
            <Grid xs={6}>
              <h2>{this.Breadcrumbs()}</h2>
            </Grid>
          </Grid>
        </Box>
        <Box height="100vh">
          <Box
            sx={{
              backgroundColor: "#F7F7F9",
              p: "20px",
              borderRadius: "10px",
              boxShadow: "rgb(76 78 100 / 22%) 0px 2px 10px 0px",
            }}
          >
            <Box sx={{ mt: "20px", ml: "20px" }}>
              <Grid container spacing={2}>
                <Grid xs={8}>
                  <h2>Input Data</h2>
                </Grid>
              </Grid>
            </Box>
            <Box className="auth-wraper">{this.InpuDataTable()}</Box>
          </Box>
        </Box>
      </Container>
    );
  }
  InpuDataTable() {
    return (
      <div>
        <div style={{ height: 400, width: "100%" }}>
          <DataGrid
            rows={this.state.rows}
            columns={this.state.columns}
            hideFooter={true}
          />
          <Button
            startIcon={<ArrowBackIcon />}
            component={RouterLink}
            to={`/details/${this.state.ref_id}`}
            sx={{ mr: "30px", textTransform: "none", mt: "30px" }}
            variant="outlined"
            size="small"
          >
            Go Back to Results
          </Button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
    runs: state.foreCasting.runs,
    refId: state.foreCasting.refId,
    project_id: state.projects.project_id,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      fetchForeCastingRuns,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(InpuDataTable);
