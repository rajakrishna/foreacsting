import * as React from "react";
import Box from "@mui/material/Box";

import CssBaseline from "@mui/material/CssBaseline";

import Toolbar from "@mui/material/Toolbar";
import Typography from "@mui/material/Typography";
import GlobalStyles from "@mui/material/GlobalStyles";
import logo from "../images/forecast-logo.png";

import {
  Avatar,
  Badge,
  Button,
  IconButton,
  ListItemIcon,
  Menu,
  MenuItem,
  Tooltip,
} from "@mui/material";
import { Help, NotificationsActive } from "@mui/icons-material";
import { Link as RouterLink } from "react-router-dom";
import { deepPurple } from "@mui/material/colors";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { userInfo } from "../store/auth/actions";
import NotificationsIcon from "@mui/icons-material/Notifications";
export interface IProps {
  userInfo: Function;
  email: any;
}
class NavBar extends React.Component<IProps, any> {
  state = {
    open: false,
  };
  componentDidMount(): void {
    this.props.userInfo();
  }
  handleClick = () => {
    if (this.state.open) {
      this.setState({ open: false });
    } else {
      this.setState({ open: true });
    }
  };
  handleClose = () => {
    this.setState({ open: false });
  };
  getFirstName = () => {
    let firstlatter = Array.from(this.props.email)[0];
    return firstlatter as string;
  };
  render() {
    return <div>{this.NavBar()}</div>;
  }

  NavBar() {
    return (
      <React.Fragment>
        <GlobalStyles
          styles={{ ul: { margin: 0, padding: 0, listStyle: "none" } }}
        />
        <CssBaseline />
        <Box border="box" borderRadius="normal" padding="medium">
          <Toolbar sx={{ flexWrap: "wrap" }}>
            <Typography
              variant="h6"
              color="inherit"
              noWrap
              sx={{ flexGrow: 1 }}
            >
              <img
                style={{ height: "50px", marginTop: "10px" }}
                src={logo}
                alt="logo"
              />
            </Typography>

            <Box
              sx={{
                display: "flex",
                alignItems: "center",
                textAlign: "center",
              }}
            >
              <Box border="box" borderRadius="normal" padding="medium">
                <Button
                  sx={{
                    ml: "20px",
                    textTransform: "none",
                  }}
                  size="small"
                ></Button>

                <br />
              </Box>
              {this.props.email && (
                <Badge color="error" variant="dot">
                  <IconButton
                    title="Notifications"
                    size="small"
                    sx={{ ml: 2, p: "0px" }}
                    component={RouterLink}
                    to="/notifications"
                  >
                    <NotificationsIcon />
                  </IconButton>
                </Badge>
              )}

              <Tooltip title="Account settings">
                <IconButton
                  onClick={this.handleClick}
                  size="small"
                  sx={{ ml: 2 }}
                  aria-controls={this.state.open ? "account-menu" : undefined}
                  aria-haspopup="true"
                  aria-expanded={this.state.open ? "true" : undefined}
                >
                  <Avatar sx={{ bgcolor: deepPurple[500] }}>
                    {this.props.email && <span>{this.getFirstName()}</span>}
                  </Avatar>
                </IconButton>
              </Tooltip>
            </Box>
            <Menu
              id="account-menu"
              open={this.state.open}
              onClose={this.handleClose}
              onClick={this.handleClose}
              PaperProps={{
                elevation: 0,
                sx: {
                  overflow: "visible",
                  filter: "drop-shadow(0px 2px 8px rgba(0,0,0,0.32))",
                  mt: 1.5,
                  "& .MuiAvatar-root": {
                    width: 32,
                    height: 32,
                    ml: -0.5,
                    mr: 1,
                  },
                  "&:before": {
                    content: '""',
                    display: "block",
                    position: "absolute",
                    top: 0,
                    right: 14,
                    width: 10,
                    height: 10,
                    bgcolor: "background.paper",
                    transform: "translateY(-50%) rotate(45deg)",
                    zIndex: 0,
                  },
                },
              }}
              transformOrigin={{ horizontal: "right", vertical: "top" }}
              anchorOrigin={{ horizontal: "right", vertical: "top" }}
            >
              {this.props.email && (
                <MenuItem component={RouterLink} to="/notifications">
                  <ListItemIcon>
                    <NotificationsActive fontSize="small" />
                  </ListItemIcon>
                  Notifications
                </MenuItem>
              )}
 <MenuItem component={RouterLink} to="/user-guide">
                <ListItemIcon>
                  <Help fontSize="small" />
                </ListItemIcon>
                User Guide
              </MenuItem>
              {this.props.email && (
                <MenuItem component={RouterLink} to="/help">
                  <ListItemIcon>
                    <Help fontSize="small" />
                  </ListItemIcon>
                  Support
                </MenuItem>
              )}
             
            </Menu>
          </Toolbar>
        </Box>
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    email: state.auth.email,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      userInfo,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
