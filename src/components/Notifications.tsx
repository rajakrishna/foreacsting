import * as React from "react";
import Grid from "@mui/material/Grid";
import Paper from "@mui/material/Paper";
import Typography from "@mui/material/Typography";
import ButtonBase from "@mui/material/ButtonBase";
import { Box, Breadcrumbs, Container, Link } from "@mui/material";

import { Link as RouterLink } from "react-router-dom";
import { NotificationsActive } from "@mui/icons-material";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { getNotifications } from "../store/notifications/actions";
import HomeIcon from "@mui/icons-material/Home";
import { updateNotification } from "../api";

export interface IProps {
  notifications: any;
  getNotifications: Function;
}
class Notifications extends React.Component<IProps, any> {
  componentDidMount(): void {
    this.props.getNotifications();
  }
  render() {
    return (
      <Container
        maxWidth="xl"
        sx={{
          backgroundColor: "#F0F2F2",
          fontFamily: "sans-serif",
          color: "rgba(76, 78, 100, 0.87)",
        }}
      >
        <Box sx={{ m: "20px", ml: "20px" }}>
          <Grid container spacing={2}>
            <Grid xs={6}>
              <h2>
                {" "}
                <div role="presentation">
                  <Breadcrumbs aria-label="breadcrumb">
                    <Link
                      underline="hover"
                      sx={{ display: "flex", alignItems: "center" }}
                      color="inherit"
                      component={RouterLink}
                      to={`/`}
                    >
                      <HomeIcon sx={{ mr: 0.5 }} fontSize="inherit" />
                      Home
                    </Link>

                    <Typography
                      sx={{ display: "flex", alignItems: "center" }}
                      color="text.primary"
                    >
                      Notifications{" "}
                    </Typography>
                  </Breadcrumbs>
                </div>
              </h2>
            </Grid>
          </Grid>
        </Box>
        <Box height="100vh">
          <Box
            sx={{
              backgroundColor: "#F7F7F9",
              p: "20px",
              borderRadius: "10px",
              boxShadow: "rgb(76 78 100 / 22%) 0px 2px 10px 0px",
            }}
          >
            <Box sx={{ mt: "20px", ml: "20px" }}>
              <Grid container spacing={2}>
                <Grid xs={8}>
                  <h2>Notifications</h2>
                </Grid>
              </Grid>
            </Box>
            <Box sx={{ m: "30px" }} className="auth-wraper">
              {this.Notifications()}
            </Box>
          </Box>
        </Box>
      </Container>
    );
  }
  localDate = (date: any) => {
    let localDate = new Date(date);
    return localDate.toLocaleDateString();
  };
  handleNavigation = (model_ref_id: any) => {};
  handleClick = (item: any) => {
    let data = ["ref_id", item.ref_id];

    updateNotification(data).then((data) => {});
  };
  Notifications() {
    return (
      <Box sx={{ m: "20px" }}>
        {this.props.notifications.map((item: any): any => (
          <Paper
            sx={{
              p: 2,
              margin: "auto",
              maxWidth: 1000,
              flexGrow: 1,
              mt: "20px",
              backgroundColor: (theme) =>
                theme.palette.mode === "dark" ? "#1A2027" : "#fff",
            }}
          >
            <Grid
              onClick={() => {
                this.handleClick(item);
              }}
              sx={{
                cursor: "pointer",
                color: "rgba(76, 78, 100, 0.87)",
                textDecoration: "none",
              }}
              component={RouterLink}
              to={`/details/${item.model_ref_id}`}
              container
              spacing={2}
            >
              <Grid item>
                <ButtonBase sx={{ width: 80, height: 80 }}>
                  <NotificationsActive fontSize="large" />
                </ButtonBase>
              </Grid>

              <Grid item xs={12} sm container>
                <Grid item xs container direction="column" spacing={2}>
                  <Grid
                    item
                    xs
                    sx={item.status === "NEW" ? { fontWeight: 700 } : {}}
                  >
                    <Typography
                      sx={
                        item.status === "NEW"
                          ? { mt: "10px", fontWeight: 700 }
                          : { mt: "10px" }
                      }
                      gutterBottom
                      variant="subtitle1"
                      component="div"
                    >
                      <div>{item.message}</div>
                    </Typography>
                    <Typography variant="body2" gutterBottom>
                      Received on: {this.localDate(item.created_at)};{" "}
                      {item.status}
                    </Typography>
                  </Grid>
                </Grid>
                <Grid item>
                  <Typography variant="subtitle1" component="div"></Typography>
                </Grid>
              </Grid>
            </Grid>
          </Paper>
        ))}
      </Box>
    );
  }
}
const mapStateToProps = (state: any) => {
  return {
    notifications: state.notifications.notifications,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      getNotifications,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Notifications);
