import { DataGrid, GridColDef } from "@mui/x-data-grid";

const columns: GridColDef[] = [
  { field: "id", headerName: "Sl.No.", width: 170 },
  { field: "sku", headerName: "SKU ID", width: 230 },
  { field: "forecast", headerName: "Forecast", width: 100 },
  { field: "date", headerName: "Date", width: 330 },
  { field: "actual", headerName: "Actual", width: 330 },
];

export default function ResultsDataTables(forcastResults: any) {
  let modelList: any[] = [];
  let obj = {};
  forcastResults.forcastResults.forEach((item: any, index: any) => {
    const date = new Date(item.date);
    obj = {
      actual: item.actual,
      id: index + 1,
      date: date.toLocaleString(),
      forecast: item.forecast,
      sku: item.sku,
    };

    modelList.push(obj);
  });

  return (
    <div style={{ height: 400, width: "100%" }}>
      {modelList ? (
        <DataGrid rows={modelList} columns={columns} />
      ) : (
        <span></span>
      )}
    </div>
  );
}
