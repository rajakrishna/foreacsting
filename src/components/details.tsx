import React from "react";

import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { Box, Form, H2, Panel, Small } from "@bigcommerce/big-design";
import ReactECharts from "echarts-for-react";
import { Link as RouterLink } from "react-router-dom";

import { Alert, AlertTitle, Button, Grid, Stack } from "@mui/material";
import { DataGrid, GridColDef } from "@mui/x-data-grid";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";
import DownloadIcon from "@mui/icons-material/Download";

import CircularStatic from "./CircularProgressWithLabel";

import { login, userInfo } from "../store/auth/actions";
import { getForecastResults, GetModelDetails } from "../api";

export interface IProps {
  login: Function;
  userInfo: Function;
  token: any;
  runs: any;
}

class Details extends React.Component<IProps, any> {
  state = {
    type: "linechart",
    forecast_horizon: "W",
    runs: [],
    startDate: "12/06/2022",
    endDate: "12/06/2022",
    status: "",
    resultsData: [],
    xAxisData: [],
    uploadedActualData: [],
    results: [],
    actualData: [],
    forcastResults: [],
    skuIds: [],
    data_horizon: "",
    created_by: "",
    created_at: "",
    updated_at: "",
    started_at: "",
    finished_at: "",
    mapescore: "",
    duration: "",
  };
  componentDidMount() {
    this.getData();
  }
  pageRefresh = () => {
    setTimeout(() => {
      this.getData();
    }, 5000);
  };
  getData = () => {
    let id = window.location.href.substring(
      window.location.href.lastIndexOf("/") + 1
    );

    GetModelDetails(id).then((res) => {
      const data = res.data;
      const updated_at = new Date(data.updated_at + "Z");
      const created_at = new Date(data.created_at + "Z");

      this.getRunDetails(data.versions[0].ref_id);

      if (
        data.versions[0].status === "CREATED" ||
        data.versions[0].status === "RUNNING"
      ) {
        this.pageRefresh();
      } else {
        this.forecastResultsData(data.ref_id);
      }

      this.setState({
        updated_at: updated_at.toLocaleString(),
        created_at: created_at.toLocaleString(),
        created_by: data.created_by,
        ref_id: data.versions[0].ref_id,
        status: data.versions[0].status,
        status_message: data.versions[0].status_message,
      });
      if (data.versions[0].performance_metrics != null) {
        let jsonObject = JSON.parse(data.versions[0].performance_metrics);
        this.setState({ mapescore: jsonObject[0].mape_best });
      }
    });
  };

  forecastResultsData = (id: any) => {
    let xAxisData: any[] = [];
    let actualData: any[] = [];
    let uploadedActualData: any[] = [];
    let results: any[] = [];

    getForecastResults(id).then((res) => {
      const data = res.data;
      let flag = 1;
      let modelList: any[] = [];
      let obj = {};

      data.forEach((item: any, index: number) => {
        if (item.sku === data[0].sku) {
          let date = new Date(item.date);
          xAxisData.push(date.toDateString());
          obj = {
            actual: item.actual,
            id: index + 1,
            date: date.toLocaleString(),
            forecast: item.forecast,
            sku: item.sku,
          };
          modelList.push(obj);

          if (item.forecast && item.actual) {
            actualData.push("0");
            if (flag === 1) {
              uploadedActualData.push("0");
              uploadedActualData.push(actualData[actualData.length - 1]);
            }
            uploadedActualData.push(item.actual);
          } else {
            uploadedActualData.push("");
            actualData.push(item.actual);
          }

          if (item.forecast !== 0) {
            if (flag === 1) {
              results.push(actualData[actualData.length - 1]);
            }
            results.push(item.forecast);
            flag = 0;
          } else {
            results.push("");
          }
        }
      });
      this.setState({
        xAxisData: xAxisData,
        forcastResults: res.data,
        resultsData: modelList,
        results: results,
        actualData: actualData,
        uploadedActualData: uploadedActualData,
      });
    });
  };
  getRunDetails = (id: any) => {
    this.props.runs.forEach((item: any) => {
      item.versions.forEach((item3: any) => {
        if (item3.ref_id === id) {
          let started_at = new Date(item3.started_at + "Z");
          let finished_at = new Date(item3.finished_at + "Z");

          this.setState({
            duration: this.duration(item3.started_at, item3.finished_at),
            started_at: started_at.toLocaleString(),
            finished_at: finished_at.toLocaleString(),
          });

          item3.parameters.input_columns.forEach((item2: any) => {
            if (item2.time_col) {
              this.setState({ time_col: item2.time_col });
            }

            if (item2.sku_col) {
              this.setState({ sku_col: item2.sku_col });
            }
            if (item2.sales_col) {
              this.setState({ sales_col: item2.sales_col });
            }
            if (item2.timestamp_format) {
              this.setState({ timestamp_format: item2.timestamp_format });
            }
            if (item2.forecast_period) {
              this.setState({ forecast_period: item2.forecast_period });
            }
            if (item2.data_frequency) {
              this.setState({ data_frequency: item2.data_frequency });
            }
            if (item2.forecast_horizon) {
              this.setState({ data_horizon: item2.forecast_horizon });
            }
            if (item2.sku) {
              this.setState({ skuIds: item2.sku.split(",") });
            }
          });
        }
      });
    });
  };
  handleChange = (e: any) => {
    console.log("value", e);
    let skus = [];
    skus = e;
    this.setState({ skus: skus });
  };
  handleBarChart = () => {
    this.setState({ type: "barchart" });
  };
  handleLineChart = () => {
    this.setState({ type: "linechart" });
  };
  handleTabular = () => {
    this.setState({ type: "tabular" });
  };
  dataHorizon = (horizon: any) => {
    if (horizon === "W") {
      return "Weekly";
    } else if (horizon === "M") {
      return "Monthly";
    } else if (horizon === "D") {
      return "Daily";
    } else if (horizon === "Y") {
      return "Yearly";
    }
  };
  duration = (startTime: any, endTime: any) => {
    let diffMs = new Date(endTime).valueOf() - new Date(startTime).valueOf();

    let diffDays = Math.floor(diffMs / 86400000);
    let diffHrs = Math.floor((diffMs % 86400000) / 3600000);
    let diffMins = Math.round(((diffMs % 86400000) % 3600000) / 60000);
    let secMins = Math.round((((diffMs % 86400000) % 3600000) % 60000) / 1000);

    if (diffDays !== 0) {
      let duration =
        diffDays + " days, " + diffHrs + " hours, " + diffMins + " minutes";
      return duration;
    } else if (diffDays === 0 && diffHrs !== 0) {
      let duration = diffHrs + " hours, " + diffMins + " minutes";
      return duration;
    } else {
      let duration = diffMins + " minutes " + secMins + " secounds";
      return duration;
    }
  };
  toFixed = (mape: any) => {
    let mapeDecimal = parseFloat(mape);
    return mapeDecimal.toFixed(2);
  };
  exportToCsv = (e: any) => {
    e.preventDefault();

    let headers = ["SKU_ID,Actual,Date,Forecast"];

    let ResultsCsv = this.state.forcastResults.reduce(
      (acc: string[] = [], item) => {
        const { sku, actual, date, forecast } = item;
        acc.push([sku, actual, date, forecast].join(","));
        return acc;
      },
      []
    );

    const blob = new Blob([[...headers, ...ResultsCsv].join("\n")], {
      type: "text/csv",
    });

    const a = document.createElement("a");
    a.download = "forecasting-results.csv";
    a.href = window.URL.createObjectURL(blob);
    const clickEvt = new MouseEvent("click", {
      view: window,
      bubbles: true,
      cancelable: true,
    });
    a.dispatchEvent(clickEvt);
    a.remove();
  };
  ResultsDataTables() {
    const columns: GridColDef[] = [
      { field: "id", headerName: "Sl.No.", width: 170 },
      { field: "sku", headerName: "SKU ID", width: 230 },
      { field: "forecast", headerName: "Forecast", width: 100 },
      { field: "date", headerName: "Date", width: 330 },
      { field: "actual", headerName: "Actual", width: 330 },
    ];

    return (
      <div style={{ height: 400, width: "100%" }}>
        {this.state.resultsData ? (
          <DataGrid rows={this.state.resultsData} columns={columns} />
        ) : (
          <span></span>
        )}
      </div>
    );
  }
  forecastResults() {
    const option = {
      title: {
        text: "",
      },
      tooltip: {
        trigger: "axis",
      },
      legend: {
        data: ["Bar Chart"],
      },
      xAxis: {
        data: this.state.xAxisData,
        boundaryGap: false,
      },
      yAxis: {
        boundaryGap: [0, "100%"],
      },
      dataZoom: [
        {
          type: "inside",
          start: this.state.forcastResults.length / 4,
          end: this.state.forcastResults.length,
        },
        {
          start: this.state.forcastResults.length / 4,
          end: this.state.forcastResults.length,
        },
      ],
      series: [
        {
          name: "Actual Sales",
          type: "bar",
          data: this.state.actualData,
          showBackground: true,
          backgroundStyle: {
            color: "rgba(180, 180, 180, 0.2)",
          },
        },
        {
          name: "Forecasted Sales",
          type: "bar",
          data: this.state.results,
          showBackground: true,
          backgroundStyle: {
            color: "rgba(180, 180, 180, 0.2)",
          },
        },
        {
          name: "Actual Data",
          type: "line",
          data: this.state.uploadedActualData,
          showBackground: true,
          showSymbol: false,
          backgroundStyle: {
            color: "rgba(180, 180, 180, 0.2)",
          },
        },
      ],
    };

    const linechartoption = {
      title: {
        text: "",
      },
      tooltip: {
        trigger: "axis",
      },
      legend: {
        data: ["Line Chart"],
      },
      xAxis: {
        data: this.state.xAxisData,
        nameLocation: "middle",
        boundaryGap: false,
      },
      yAxis: {
        boundaryGap: [0, "100%"],
      },
      dataZoom: [
        {
          type: "inside",
          start: this.state.forcastResults.length / 4,
          end: this.state.forcastResults.length,
        },
        {
          start: this.state.forcastResults.length / 4,
          end: this.state.forcastResults.length,
        },
      ],
      series: [
        {
          name: "Actual Sales ",
          type: "line",
          data: this.state.actualData,
          showBackground: true,
          showSymbol: false,
          backgroundStyle: {
            color: "rgba(180, 180, 180, 0.2)",
          },
        },
        {
          name: "Forecast Sales",
          type: "line",
          data: this.state.results,
          showBackground: true,
          showSymbol: false,
          backgroundStyle: {
            color: "rgba(180, 180, 180, 0.2)",
          },
        },
        {
          name: "Actual Data",
          type: "line",
          data: this.state.uploadedActualData,
          showBackground: true,
          showSymbol: false,
          backgroundStyle: {
            color: "rgba(180, 180, 180, 0.2)",
          },
        },
      ],
    };
    return (
      <Grid container spacing={2}>
        <Grid sx={{ p: "23px" }} xs={9}>
          <Box border="box" borderRadius="normal" padding="medium">
            {this.state.status === "SUCCESS" && (
              <Grid xs={5}>
                <Button
                  sx={{
                    ml: "20px",
                    textTransform: "none",
                  }}
                  size="small"
                  onClick={this.handleLineChart}
                >
                  Line Chart
                </Button>

                <Button
                  sx={{
                    ml: "20px",
                    textTransform: "none",
                  }}
                  size="small"
                  onClick={this.handleBarChart}
                >
                  Bar Chart{" "}
                </Button>

                <Button
                  sx={{
                    ml: "20px",
                    textTransform: "none",
                  }}
                  size="small"
                  onClick={this.handleTabular}
                >
                  Tabular
                </Button>
              </Grid>
            )}
            <Box>
              {this.state.status === "ERROR" && (
                <Box style={{ padding: "100px" }}>
                  <Stack
                    sx={{ width: "100%", mt: "20px", mb: "20px" }}
                    spacing={2}
                  >
                    <Alert variant="outlined" severity="error">
                      <AlertTitle>{this.state.status}</AlertTitle>
                      An error occurred while running the forecast; Please{" "}
                      <a href={`mailto:support@predera.com?`}>contact </a>
                      support for further assistance.
                    </Alert>
                  </Stack>
                </Box>
              )}
              <div>
                {this.state.status === "CREATED" && (
                  <Box style={{ padding: "100px" }}>
                    <Stack
                      height="100%"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <CircularStatic />
                      <strong style={{ marginTop: "30px" }}>
                        Please wait until forecast run completes, to see the
                        results.
                      </strong>
                      <Button
                        component={RouterLink}
                        to={`/`}
                        sx={{
                          mr: "30px",
                          textTransform: "none",
                          mt: "20px",
                        }}
                        variant="outlined"
                        size="small"
                      >
                        Go Back to Runs
                      </Button>
                    </Stack>
                  </Box>
                )}
                {this.state.status === "RUNNING" && (
                  <Box style={{ padding: "100px" }}>
                    <Stack
                      height="100%"
                      alignItems="center"
                      justifyContent="center"
                    >
                      <CircularStatic />
                      <strong style={{ marginTop: "30px" }}>
                        Please wait until forecast run completes, to see the
                        results.
                      </strong>
                      <Button
                        component={RouterLink}
                        to={`/`}
                        sx={{
                          mr: "30px",
                          textTransform: "none",
                          mt: "20px",
                        }}
                        variant="outlined"
                        size="small"
                      >
                        Go Back to Runs
                      </Button>
                    </Stack>
                  </Box>
                )}
              </div>
            </Box>
            {this.state.status === "SUCCESS" && (
              <Grid>
                {this.state.type === "linechart" && (
                  <ReactECharts
                    option={linechartoption}
                    style={{ height: 400 }}
                    opts={{ renderer: "svg" }}
                  />
                )}
                {this.state.type === "barchart" && (
                  <ReactECharts
                    option={option}
                    style={{ height: 400 }}
                    opts={{ renderer: "svg" }}
                  />
                )}
                {this.state.type === "tabular" && (
                  <div>{this.ResultsDataTables()}</div>
                )}
              </Grid>
            )}
          </Box>
        </Grid>
        <Grid xs={3} sx={{ pt: "23px" }}>
          <Box
            backgroundColor="secondary20"
            border="box"
            borderRadius="normal"
            padding="medium"
          >
            <H2>Configuration Details</H2>

            <Form>
              <Small>SKU :{this.state.skuIds[0]}</Small>
              <Small>Created by :{this.state.created_by}</Small>
              <Small>Created on :{this.state.created_at}</Small>
              <Small>Updated on :{this.state.updated_at}</Small>
              <Small>Start Date :{this.state.started_at}</Small>

              <Small>End Date :{this.state.finished_at}</Small>

              <Small>
                Forecast horizon: {this.dataHorizon(this.state.data_horizon)}
              </Small>
              {this.state.status === "SUCCESS" ||
                (this.state.status === "ERROR" && (
                  <Small>Duration :{this.state.duration}</Small>
                ))}

              {this.state.mapescore && (
                <Small>MAPE score :{this.toFixed(this.state.mapescore)}</Small>
              )}
              {!this.state.mapescore && <Small>MAPE score :N/A</Small>}
            </Form>
          </Box>
        </Grid>

        <Grid xs={3} sx={{ pt: "20px" }}></Grid>
      </Grid>
    );
  }
  render() {
    return (
      <div style={{ margin: "20px" }}>
        <Panel>
          <Button
            startIcon={<ArrowBackIcon />}
            variant="contained"
            sx={{
              ml: "20px",
              textTransform: "none",
              float: "left",
            }}
            size="small"
            component={RouterLink}
            to="/"
          >
            {"Back"}
          </Button>
          {this.state.status === "SUCCESS" && (
            <Button
              onClick={this.exportToCsv}
              startIcon={<DownloadIcon />}
              variant="outlined"
              sx={{
                ml: "20px",
                textTransform: "none",
                float: "right",
              }}
              size="small"
            >
              {"Download"}
            </Button>
          )}
          {this.forecastResults()}
        </Panel>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
    runs: state.foreCasting.runs,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      login,
      userInfo,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(Details);
