import React from "react";

import { Box, Container, Grid } from "@mui/material";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { login } from "../store/auth/actions";

export interface IProps {
  login: Function;
  token: any;
}

class InstallationGuide extends React.Component<IProps, any> {
  render() {
    return (
      <Container
        maxWidth="xl"
        sx={{
          backgroundColor: "#F0F2F2",
          fontFamily: "sans-serif",
          color: "rgba(76, 78, 100, 0.87)",
        }}
      >
        <Box>
          <Box
            sx={{
              backgroundColor: "#F7F7F9",
              borderRadius: "10px",
              boxShadow: "rgb(76 78 100 / 22%) 0px 2px 10px 0px",
              p: "20px",
              mt: "20px",
            }}
          >
            <Box sx={{ mt: "20px", ml: "20px" }}>
              <Grid container spacing={2}>
                <Grid xs={10}>
                  <h2> Steps to Install the app </h2>
                </Grid>
              </Grid>
            </Box>

            <Box sx={{ mt: "10px", ml: "20px" }}>
              <Grid container spacing={2}>
                <ol type="1">
                  <li style={{ marginLeft: "20px" }}>
                    You are on the <b>SalesEasy</b> app and go to app details
                    page.
                  </li>
                  <li style={{ marginLeft: "20px" }}>
                    Install the <b>SalesEasy</b> application
                  </li>
                  <li style={{ marginLeft: "20px" }}>
                    Read the app details and description.{" "}
                  </li>
                  <li style={{ marginLeft: "20px" }}>
                    Click on <b>“Get This App”</b> to start the installation
                    process.
                  </li>

                  <li style={{ marginLeft: "20px" }}>
                    Login to your Existing Big-commerce Account or Create a New
                    Big-commerce Store.
                  </li>

                  <li style={{ marginLeft: "20px" }}>
                    Once Logged in Click on <b>“Install”</b> to get the app in
                    your store.
                  </li>

                  <li style={{ marginLeft: "20px" }}>
                    Once installed user needs to confirm the installation by
                    clicking on the <b>“Confirm”</b> button generated on the
                    screen.
                  </li>

                  <li style={{ marginLeft: "20px" }}>
                    The app will now get installed on your store and you can
                    find the app on <b>Apps {"-->"} My Apps</b> Section.
                  </li>
                </ol>
              </Grid>
            </Box>
          </Box>
        </Box>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      login,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(InstallationGuide);
