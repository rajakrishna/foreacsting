import React from "react";

import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { login, userInfo } from "../store/auth/actions";
import {
  Fieldset,
  Form,
  FormGroup,
  H2,
  Panel,
  Radio,
  Small,
} from "@bigcommerce/big-design";
import { Link as RouterLink, Navigate } from "react-router-dom";

import {
  Autocomplete,
  Button,
  FormControl,
  Grid,
  TextField,
} from "@mui/material";
import { searchProducts } from "../store/bigcommerce/actions";
import { fetchProjects } from "../store/projects/actions";
import {
  createModel,
  fetchForeCastingRuns,
} from "../store/foreCasting/actions";
import { getDatasources } from "../store/datasource/actions";
export interface IProps {
  login: Function;
  userInfo: Function;
  searchProducts: Function;
  fetchProjects: Function;
  fetchForeCastingRuns: Function;
  getDatasources: Function;
  createModel: Function;
  datasourceId: any;
  project_id: any;
  token: any;
  products: [];
}

class NewForecast extends React.Component<IProps, any> {
  state = {
    skus: [],
    forecast_horizon: "W",
    products: [],
    ref_id: "",
    openSuccessSnakbar: false,
    startDate: new Date(),
    endDate: new Date(),
  };
  componentDidMount() {
    this.props.fetchProjects().then((res: any) => {
      if (res) {
        this.props.fetchForeCastingRuns(this.props.project_id);
        this.props.getDatasources(this.props.project_id);
      }
    });
    this.props.searchProducts();
    this.setState({ products: this.props.products });
  }
  handleChange = (event: any, values: any) => {
    this.setState({ skus: values });
  };
  horizonChange = (e: any) => {
    this.setState({ forecast_horizon: e.target.value });
  };
  handleStartDateChange = (e: any) => {
    this.setState({
      startDate: e.target.value,
    });
  };
  handleEndDateChange = (e: any) => {
    this.setState({
      endDate: e.target.value,
    });
  };
  getName = () => {
    let now = new Date();
    let timestamp = now.getFullYear().toString();
    timestamp += (now.getMonth() ? "0" : "") + now.getMonth().toString();
    timestamp += (now.getDate() ? "0" : "") + now.getDate().toString();
    timestamp += (now.getHours() ? "0" : "") + now.getDate().toString();
    timestamp += (now.getMinutes() ? "0" : "") + now.getDate().toString();
    timestamp += (now.getSeconds() ? "0" : "") + now.getDate().toString();
    timestamp += (now.getMilliseconds() ? "0" : "") + now.getDate().toString();
    return "forecast-" + timestamp;
  };
  getSKUs = () => {
    let array: any[] = [];
    this.state.skus.forEach((item: any) => {
      array.push(item.sku);
    });
    return array;
  };
  submitModel = () => {
    let data = {
      name: this.getName(),
      type: "Demand Forecast",
      desc: this.getName() + "description",
      data_source: this.props.datasourceId,
      project_id: this.props.project_id,
      parameters: {
        input_columns: [
          { forecast_horizon: this.state.forecast_horizon },
          { from_date: this.state.startDate },
          { end_date: this.state.endDate },
          { sku: this.getSKUs() },
        ],
        target_column: "sales",
      },
    };
    this.props.createModel({ ...data }).then((res: any) => {
      if (res) {
        this.props.fetchForeCastingRuns(this.props.project_id);
        this.setState({
          ref_id: res.ref_id,
          openSuccessSnakbar: true,
          message: "The forecast has been created",
        });
      }
    });
  };
  render() {
    return this.state.openSuccessSnakbar ? (
      <Navigate to={`/details/${this.state.ref_id}`} replace />
    ) : (
      <div style={{ margin: "20px" }}>
        <Panel>
          <H2>Configuration Details</H2>
          <Form>
            <Small>Select Product(s)</Small>

            <FormGroup>
              <Autocomplete
                multiple
                filterSelectedOptions
                id="combo-box-demo"
                options={this.props.products}
                getOptionLabel={(option) => option["name"]}
                onChange={this.handleChange}
                sx={{ width: 300 }}
                renderInput={(params) => (
                  <TextField
                    {...params}
                    label="Select Product"
                    variant="outlined"
                    size="small"
                  />
                )}
              />
            </FormGroup>
          </Form>
          <br />
          <Small>Recorded Data</Small>
          <Grid container spacing={3}>
            <Grid item xs={3}>
              <FormControl sx={{ mt: "20px", width: "100%" }} size="small">
                <TextField
                  label="Start Date"
                  name="startDate"
                  onChange={this.handleStartDateChange}
                  value={this.state.startDate}
                  InputLabelProps={{ shrink: true, required: true }}
                  type="date"
                  defaultValue={this.state.startDate}
                  size="small"
                  variant="standard"
                />
              </FormControl>{" "}
            </Grid>
            <Grid item xs={3}>
              <FormControl sx={{ mt: "20px", width: "100%" }} size="small">
                <TextField
                  label="End Date"
                  name="endDate"
                  onChange={this.handleEndDateChange}
                  InputLabelProps={{ shrink: true, required: true }}
                  type="date"
                  value={this.state.endDate}
                  defaultValue={this.state.endDate}
                  size="small"
                  variant="standard"
                />
              </FormControl>{" "}
            </Grid>
          </Grid>
          <br />
          <Small>Forecast horizon</Small>
          <FormGroup>
            <Grid container spacing={2}>
              <Grid xs={3} sx={{ pt: "20px", ml: "15px" }}>
                <Fieldset>
                  <FormGroup>
                    <Radio
                      checked={this.state.forecast_horizon === "D"}
                      onChange={this.horizonChange}
                      label="Daily"
                      value="D"
                    />
                  </FormGroup>
                </Fieldset>
              </Grid>
              <Grid xs={3} sx={{ pt: "20px" }}>
                <Fieldset>
                  <FormGroup>
                    <Radio
                      checked={this.state.forecast_horizon === "W"}
                      onChange={this.horizonChange}
                      label="Weekly"
                      value="W"
                    />
                  </FormGroup>
                </Fieldset>
              </Grid>
              <Grid xs={2} sx={{ pt: "20px" }}>
                <Fieldset>
                  <FormGroup>
                    <Radio
                      checked={this.state.forecast_horizon === "M"}
                      onChange={this.horizonChange}
                      label="Monthly"
                      value="M"
                    />
                  </FormGroup>
                </Fieldset>
              </Grid>
              <Grid xs={3} sx={{ pt: "20px" }}>
                <Fieldset>
                  <FormGroup>
                    <Radio
                      checked={this.state.forecast_horizon === "Y"}
                      onChange={this.horizonChange}
                      label="Yearly"
                      value="Y"
                    />
                  </FormGroup>
                </Fieldset>
              </Grid>
            </Grid>
          </FormGroup>
          <Button
            component={RouterLink}
            to={`/`}
            sx={{ mr: "30px", textTransform: "none", mt: "20px" }}
            variant="outlined"
            size="small"
          >
            Cancel
          </Button>
          <Button
            variant="contained"
            sx={{ textTransform: "none", mt: "20px" }}
            size="small"
            onClick={this.submitModel}
          >
            Save
          </Button>
        </Panel>
      </div>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
    products: state.bigcommerce.products,
    datasourceId: state.datasource.datasourceId,
    project_id: state.projects.project_id,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      login,
      userInfo,
      searchProducts,
      fetchProjects,
      fetchForeCastingRuns,
      getDatasources,
      createModel,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(NewForecast);
