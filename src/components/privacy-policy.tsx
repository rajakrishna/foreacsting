import React from "react";

import { Box, Container, Grid } from "@mui/material";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { login } from "../store/auth/actions";
import { HR, Text } from "@bigcommerce/big-design";

export interface IProps {
  login: Function;
  token: any;
}

class PrivacyPolicy extends React.Component<IProps, any> {
  render() {
    return (
      <Container
        maxWidth="xl"
        sx={{
          backgroundColor: "#F0F2F2",
          fontFamily: "sans-serif",
          color: "rgba(76, 78, 100, 0.87)",
        }}
      >
        <Grid container spacing={2}>
        <Grid xs={2}></Grid>
          <Grid xs={8}>
            <Box>
              <Box
                sx={{
                  backgroundColor: "#F7F7F9",
                  borderRadius: "10px",
                  boxShadow: "rgb(76 78 100 / 22%) 0px 2px 10px 0px",
                  p: "20px",
                  mt: "20px",
                  maxWidth: "1024px",
                }}
              >
                <Box sx={{ mt: "20px", ml: "20px" }}>
                  <h2 style={{ textAlign: "center" }}>
                    {" "}
                    Privacy Policy for SalesEasy{" "}
                  </h2>
                </Box>
                <Box sx={{ m: "20px", ml: "20px" }}>
                  <HR />
                </Box>
                <Box sx={{ mt: "30px", ml: "35px" }}>
                  <Grid container spacing={2}>
                    <Text style={{ margin: "30px" }}>
                      Welcome to SalesEasy, an AI-based forecasting application
                      for businesses available on the BigCommerce marketplace.
                      Your privacy is important to us, and we are committed to
                      protecting it through our compliance with this privacy
                      policy.
                    </Text>
                    <Text style={{ margin: "30px" }}>
                      This privacy policy describes the types of information we
                      may collect from you or that you may provide when you use
                      our application, and our practices for collecting, using,
                      maintaining, protecting, and disclosing that information.
                    </Text>
                    <h4>Information We Collect:</h4>

                    <ul style={{ listStyleType: "disc", marginLeft: "30px" }}>
                      <li>
                        <Text style={{ margin: "20px" }}>
                          Personal information: We may collect personal
                          information such as your name, email address, and
                          contact information when you create an account or
                          contact us for support.
                        </Text>
                      </li>
                      <li>
                        <Text style={{ margin: "20px" }}>
                          Forecasting data: We may collect data related to your
                          sales and inventory, such as product and customer
                          information, sales figures, and other data that you
                          provide in order to generate forecasts.
                        </Text>
                      </li>
                    </ul>
                    <Box>
                      <h4>Use of Information: </h4>

                      <ul style={{ listStyleType: "disc", marginLeft: "30px" }}>
                        <li style={{ marginTop: "30px" }}>
                          <Text style={{ margin: "20px" }}>
                            We use the information we collect to provide and
                            improve the application, including to:
                          </Text>
                          <ul
                            style={{
                              listStyleType: "circle",
                              marginLeft: "30px",
                            }}
                          >
                            <li>
                              <Text style={{ margin: "20px" }}>
                                Provide and deliver the features and
                                functionality of the application, such as
                                generating forecasts.
                              </Text>
                            </li>
                            <li>
                              <Text style={{ margin: "20px" }}>
                                Communicate with you, such as to respond to your
                                requests for support.{" "}
                              </Text>
                            </li>
                            <li>
                              <Text style={{ margin: "20px" }}>
                                Monitor and analyze usage and trends to improve
                                the application.{" "}
                              </Text>
                            </li>
                          </ul>
                        </li>
                      </ul>
                    </Box>
                    <Box>
                      <h4>Sharing of Information: </h4>
                      <ul style={{ listStyleType: "disc", marginLeft: "30px" }}>
                        <li>
                          <Text style={{ margin: "20px" }}>
                            We may share your information with third parties who
                            provide services to us, such as hosting and
                            analytics providers. These third parties are bound
                            by contractual obligations to keep your information
                            confidential and use it only for the purposes for
                            which we disclose it to them.
                          </Text>
                        </li>
                        <li>
                          <Text style={{ margin: "20px" }}>
                            We may also disclose your information as required by
                            law, such as to comply with a subpoena or similar
                            legal process.
                          </Text>
                        </li>
                      </ul>
                    </Box>
                    <Box>
                      <h4>Security: </h4>
                      <ul style={{ listStyleType: "disc", marginLeft: "30px" }}>
                        <li>
                          <Text style={{ margin: "20px" }}>
                            We take reasonable measures to protect your
                            information from unauthorized access, use, or
                            disclosure. However, no method of transmission over
                            the internet, or method of electronic storage, is
                            100% secure. Therefore, while we strive to use
                            commercially acceptable means to protect your
                            information, we cannot guarantee its absolute
                            security.
                          </Text>
                        </li>
                      </ul>
                    </Box>
                    <Box>
                      <h4>Changes to Our Privacy Policy: </h4>
                      <ul style={{ listStyleType: "disc", marginLeft: "30px" }}>
                        <li>
                          <Text style={{ margin: "20px" }}>
                            We may update this privacy policy from time to time
                            to reflect changes to our information practices. If
                            we make any material changes, we will notify you by
                            email or by means of a notice on the application
                            prior to the change becoming effective.
                          </Text>
                        </li>
                      </ul>
                    </Box>
                    <Box>
                      <h4>Contact Us: </h4>
                      <ul style={{ listStyleType: "disc", marginLeft: "30px" }}>
                        <li>
                          <Text style={{ margin: "20px" }}>
                            If you have any questions or concerns about our
                            privacy policy, please contact us at{" "}
                            <a href={`mailto:privacy@predera.com?`}>
                              privacy@predera.com.{" "}
                            </a>
                          </Text>
                        </li>
                      </ul>
                    </Box>
                    <Box style={{ marginTop: "30px" }}>
                      <Text>
                        By using SalesEasy, you acknowledge that you have read
                        and understand this privacy policy and agree to be bound
                        by its terms.
                      </Text>
                      <Text>Effective date: 16-Jan-2023</Text>
                    </Box>
                  </Grid>
                </Box>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      login,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy);
