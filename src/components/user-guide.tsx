import React from "react";

import { Box, Container, Grid } from "@mui/material";
import { connect } from "react-redux";
import { ThunkDispatch } from "redux-thunk";
import { AnyAction, bindActionCreators } from "redux";
import { login } from "../store/auth/actions";
import home from "../images/home.jpg";
import home2 from "../images/home2.jpg";
import home3 from "../images/home3.jpg";
import home4 from "../images/home4.jpg";
import home5 from "../images/home5.jpg";
import home6 from "../images/home6.jpg";
import home7 from "../images/home7.jpg";
import home8 from "../images/home8.jpg";
import home9 from "../images/home9.jpg";
import home10 from "../images/home10.jpg";

export interface IProps {
  login: Function;
  token: any;
}

class UserGuide extends React.Component<IProps, any> {
  render() {
    return (
      <Container
        maxWidth="xl"
        sx={{
          backgroundColor: "#F0F2F2",
          fontFamily: "sans-serif",
          color: "rgba(76, 78, 100, 0.87)",
        }}
      >
        <Box>
          <Box
            sx={{
              backgroundColor: "#F7F7F9",
              borderRadius: "10px",
              boxShadow: "rgb(76 78 100 / 22%) 0px 2px 10px 0px",
              p: "20px",
              mt: "20px",
            }}
          >
            <Box sx={{ mt: "20px", ml: "20px" }}>
              <Grid container spacing={2}>
                <Grid xs={10}>
                  <h2> A Guide for Users of SalesEasy</h2>
                </Grid>
              </Grid>
            </Box>

            <Box sx={{ mt: "10px", ml: "20px" }}>
              <Grid container spacing={2}>
                <ol type="1">
                  <h4> Home Page (Dashboard)</h4>

                  <li style={{ marginLeft: "20px" }}>
                    To start with the application the app starts with fetching
                    the product details.{" "}
                  </li>
                  <li style={{ marginLeft: "20px" }}>
                    The user can start by creating a New Forecast by clicking on
                    the <b>"Create New Forecast"</b> button.
                  </li>
                  <img style={{ margin: "20px" }} src={home} alt="home" />
                  <li style={{ marginLeft: "20px" }}>
                    The next step would be selecting the product from the
                    drop-down option and the time horizon for which the forecast
                    needs to be scheduled.
                  </li>
                  <img style={{ margin: "20px" }} src={home2} alt="home2" />

                  <li style={{ marginLeft: "20px" }}>
                    The running forecasts and newly created forecasts are saved
                    on the Home-page {"-->"} Active Section {"-->"} Dashboard
                    table.
                  </li>
                  <img style={{ margin: "20px" }} src={home3} alt="home3" />

                  <li style={{ marginLeft: "20px" }}>
                    Users can access the forecast details from the Home-page{" "}
                    {"-->"} Active Section {"-->"} Dashboard table by clicking
                    on the “View” button associated with the particular
                    forecast.
                  </li>
                  <img style={{ margin: "20px" }} src={home4} alt="home4" />

                  <h4> Forecast Details Page</h4>

                  <li style={{ marginLeft: "20px" }}>
                    Forecast graphs consider historical data and forecast data
                    to give users meaningful results.
                  </li>
                  <img style={{ margin: "20px" }} src={home5} alt="home5" />

                  <li style={{ marginLeft: "20px" }}>
                    The forecast graph can be zoomed out/zoomed in to get a
                    broader view of the forecast.{" "}
                  </li>
                  <img style={{ margin: "20px" }} src={home6} alt="home6" />

                  <li style={{ marginLeft: "20px" }}>
                    Users can also change the graph visualization format to a
                    Line chart, Bar graph and Tabular data format for ease of
                    understanding.{" "}
                  </li>
                  <img style={{ margin: "20px" }} src={home7} alt="home7" />

                  <li style={{ marginLeft: "20px" }}>
                    Users can download a forecast result in CSV format to easily
                    share it between their team.{" "}
                  </li>
                  <img style={{ margin: "20px" }} src={home8} alt="home8" />

                  <li style={{ marginLeft: "20px" }}>
                    The app is capable of generating a forecast comparison
                    between actual sales and forecasted sales. Once the
                    forecasted sales time period has passed and actual sales are
                    generated for that particular time.{" "}
                  </li>
                  <h4>Forecast Scheduling</h4>
                  <li style={{ marginLeft: "20px" }}>
                    Once a forecast is created, it is scheduled according to the
                    horizon selected and repeats the forecast cycle.
                  </li>
                  <li style={{ marginLeft: "20px" }}>
                    In order to stop a forecast, the user needs to delete it.{" "}
                  </li>
                  <img style={{ margin: "20px" }} src={home9} alt="home9" />

                  <li style={{ marginLeft: "20px" }}>
                    All inactive forecasts are saved in the Home-page {"-->"}{" "}
                    Inactive Section {"-->"} Dashboard table, from where past
                    forecast information can be retrieved.
                  </li>
                  <img style={{ margin: "20px" }} src={home10} alt="home10" />
                </ol>
              </Grid>
            </Box>
          </Box>
        </Box>
      </Container>
    );
  }
}

const mapStateToProps = (state: any) => {
  return {
    token: state.auth.token,
  };
};

const mapDispatchToProps = (dispatch: ThunkDispatch<any, any, AnyAction>) => {
  return bindActionCreators(
    {
      login,
    },
    dispatch
  );
};
export default connect(mapStateToProps, mapDispatchToProps)(UserGuide);
