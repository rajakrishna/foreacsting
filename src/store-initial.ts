import {
    combineReducers,
    configureStore,
    PreloadedState
  } from '@reduxjs/toolkit'
import { authReducer } from './store/auth/reducer'
import { datasourceReducer } from './store/datasource/reducer'
import { feedBackReducer } from './store/feedback/reducer'
import { foreCastingReducer } from './store/foreCasting/reducer'
import { notificationsReducer } from './store/notifications/reducer'
  // Create the root reducer independently to obtain the RootState type
 export const rootReducer = combineReducers({
    datasource:datasourceReducer,
    foreCasting: foreCastingReducer,
    notifications:notificationsReducer,
    feedback:feedBackReducer,
    auth: authReducer,
  })
  export function setupStore(preloadedState?: PreloadedState<RootState>) {
    return configureStore({
      reducer: rootReducer,
      preloadedState
    })
  }
  export type RootState = ReturnType<typeof rootReducer>
  export type AppStore = ReturnType<typeof setupStore>
  export type AppDispatch = AppStore['dispatch']