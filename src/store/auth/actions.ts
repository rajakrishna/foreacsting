import { AuthActionTypes } from "./types";

import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";
import { getUserInfo, login as userLogin } from "../../api";

import { ApplicationState } from "../index";

export type AppThunk = ThunkAction<
  void,
  ApplicationState,
  null,
  Action<string>
>;

export const login: AppThunk = (loginDetails: any) => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    userLogin(loginDetails)
      .then((response: any) => {
        const data = response.data;
        localStorage.setItem("token", data.token);
        sessionStorage.setItem("token", data.token);

        return dispatch({
          type: AuthActionTypes.AUTH_LOGIN_SUCCESS,
          payload: data.token,
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: AuthActionTypes.AUTH_LOGIN_ERROR,
        });
      });
  };
};
export const userInfo : AppThunk = () => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    getUserInfo()
      .then((response: any) => {
        const data = response.data;
    
        return dispatch({
          type: AuthActionTypes.GET_USERINFO_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: AuthActionTypes.GET_USERINFO_ERROR,
        });
      });
  };
};
export const logout: AppThunk = () => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    return dispatch({
      type: AuthActionTypes.AUTH_LOGOUT,
      payload: "",
    });
  };
};
