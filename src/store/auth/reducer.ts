import { Reducer } from "redux";
import { loadToken } from "../../utils/storage";

import { AuthActionTypes, auth } from "./types";
export const initialState: any = {
  runs: [],
  token: loadToken(),
  errors: undefined,
  loading: false,
  userInfo: {},
  email: null,
};

const reducer: Reducer<auth> = (state = initialState, action) => {
  switch (action.type) {
    case AuthActionTypes.AUTH_LOGIN: {
      return { ...state, loading: true };
    }
    case AuthActionTypes.AUTH_LOGIN_SUCCESS: {
      return { ...state, loading: false, token: action.payload };
    }
    case AuthActionTypes.AUTH_LOGIN_ERROR: {
      return { ...state, loading: false, errors: action.payload };
    }
    case AuthActionTypes.AUTH_LOGOUT: {
      return { ...state, loading: false, token: null };
    }
    case AuthActionTypes.GET_USERINFO_SUCCESS: {
      return {
        ...state,
        loading: false,
        userInfo: action.payload,
        email: action.payload.email,
      };
    }
    default: {
      return state;
    }
  }
};

export { reducer as authReducer };
