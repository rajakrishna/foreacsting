export enum AuthActionTypes {
  AUTH_LOGIN = "@@auth/AUTH_LOGIN",
  AUTH_LOGIN_ERROR = "@@auth/AUTH_LOGIN_ERROR",
  AUTH_LOGIN_SUCCESS = "@@auth/AUTH_LOGIN__SUCCESS",
  AUTH_LOGOUT = "@@auth/AUTH_LOGOUT",
  GET_USERINFO_SUCCESS = "@@auth/GET_USERINFO_SUCCESS",
  GET_USERINFO_ERROR = "@@auth/GET_USERINFO_ERROR",
}

export interface auth {
  readonly loading: boolean;
  readonly data: any;
  readonly errors?: string;
}
