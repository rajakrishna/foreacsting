import { BigCommerceTypes } from "./types";

import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";

import { ApplicationState } from "../index";
import { getProducts } from "../../api";

export type AppThunk = ThunkAction<
  void,
  ApplicationState,
  null,
  Action<string>
>;
type ThunkResult<R> = ThunkAction<R, any, any, any>;

export function searchProducts(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await getProducts(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: BigCommerceTypes.SEARCH_PRODUCT_SUCCESS,
          payload: data.data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: BigCommerceTypes.SEARCH_PRODUCT_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export const searchProducts1: AppThunk = (data?: any) => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    getProducts(data)
      .then((response: any) => {
        const data = response.data;

        return dispatch({
          type: BigCommerceTypes.SEARCH_PRODUCT_SUCCESS,
          payload: data.data,
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: BigCommerceTypes.SEARCH_PRODUCT_ERROR,
        });
      });
  };
};
