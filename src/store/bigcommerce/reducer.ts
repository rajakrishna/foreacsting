import { Reducer } from "redux";
import { BigCommerceState, BigCommerceTypes } from "./types";

export const initialState: any = {
    products:[],
    errors: false,
    loading: false,
  };
  const reducer: Reducer<BigCommerceState> = (state = initialState, action) => {

    switch (action.type) {
      case BigCommerceTypes.SEARCH_PRODUCT: {
        return { ...state, loading: true,errors:false };
      }
      case BigCommerceTypes.SEARCH_PRODUCT_SUCCESS: {
        return { ...state, loading: false, errors: false,products:action.payload };
      }
     
      case BigCommerceTypes.SEARCH_PRODUCT_ERROR: {
        return { ...state, loading: false, errors:true,products:[]};
      }
      default: {
        return state;
      }
    }
  };
  export { reducer as BigCommerceReducer };
