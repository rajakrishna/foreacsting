export interface BigCommerce {
    data: {};
  }
  export enum BigCommerceTypes {
    SEARCH_PRODUCT = "@@feedback/SEARCH_PRODUCT",
    SEARCH_PRODUCT_SUCCESS = "@@feedback/SEARCH_PRODUCT_SUCCESS",
    SEARCH_PRODUCT_ERROR = "@@feedback/SEARCH_PRODUCT_ERROR",
  }
  
  export interface BigCommerceState {
    readonly loading: boolean;
    readonly data: BigCommerce;
    readonly errors?: boolean;
  }