import { DatasourcesTypes } from "./types";

import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";

import { ApplicationState } from "../index";
import {
  createSource,
  deleteSource,
  getAllSources,
  getSampleRecords,
  updateSource,
} from "../../api";

export type AppThunk = ThunkAction<
  void,
  ApplicationState,
  null,
  Action<string>
>;
type ThunkResult<R> = ThunkAction<R, any, any, any>;

export const getDatasources: AppThunk = (id: any) => {
  let resposeData: null = null;

  return async (dispatch: Dispatch, state: ApplicationState): Promise<any> => {
    await getAllSources(id)
      .then((response: any) => {
        const data = response.data;

        return dispatch({
          type: DatasourcesTypes.FETCH_DATASORCES_SUCCESS,
          payload: data.reverse(),
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: DatasourcesTypes.FETCH_DATASORCES_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
};

export function saveeDatasource(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await createSource(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: DatasourcesTypes.CREATE_DATASORCES_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: DatasourcesTypes.CREATE_DATASORCES_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export function updateDatasource(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await updateSource(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: DatasourcesTypes.UPDATE_DATASORCES_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: DatasourcesTypes.UPDATE_DATASORCES_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export function deleteDatasource(id: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await deleteSource(id)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: DatasourcesTypes.DELETE_DATASORCES_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: DatasourcesTypes.DELETE_DATASORCES_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export const getDatasourceSampleRecords: AppThunk = (id: any) => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    getSampleRecords(id)
      .then((response: any) => {
        const data = response.data;

        return dispatch({
          type: DatasourcesTypes.FETCH_SAMPLE_RECORDS_SUCCESS,
          payload: data.reverse(),
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: DatasourcesTypes.FETCH_DATASORCES_ERROR,
        });
      });
  };
};
