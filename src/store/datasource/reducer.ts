import { Reducer } from "redux";
import { DatasourcesTypes, datasourcesState } from "./types";

export const initialState: any = {
  datasources: [],
  sampleRecords:[],
  datasourceId:"",
  errors: false,
  loading: false,
};
const reducer: Reducer<datasourcesState> = (state = initialState, action) => {
  switch (action.type) {
    case DatasourcesTypes.FETCH_DATASORCES: {
      return { ...state, loading: true, errors: false };
    }
    case DatasourcesTypes.FETCH_DATASORCES_SUCCESS: {
      return {
        ...state,
        loading: false,
        errors: false,
        datasources: action.payload,
        datasourceId:action.payload[0].ref_id
      };
    }

    case DatasourcesTypes.FETCH_DATASORCES_ERROR: {
      return { ...state, loading: false, errors: true };
    }
    case DatasourcesTypes.CREATE_DATASORCES: {
      return { ...state, loading: true, errors: false };
    }
    case DatasourcesTypes.CREATE_DATASORCES_SUCCESS: {
      return { ...state, loading: false, errors: false };
    }

    case DatasourcesTypes.CREATE_DATASORCES_ERROR: {
      return { ...state, loading: false, errors: true };
    }
    case DatasourcesTypes.DELETE_DATASORCES: {
      return { ...state, loading: true, errors: false };
    }
    case DatasourcesTypes.DELETE_DATASORCES_SUCCESS: {
      return { ...state, loading: false, errors: false };
    }

    case DatasourcesTypes.DELETE_DATASORCES_ERROR: {
      return { ...state, loading: false, errors: true };
    }
    case DatasourcesTypes.UPDATE_DATASORCES: {
      return { ...state, loading: true, errors: false };
    }
    case DatasourcesTypes.UPDATE_DATASORCES_SUCCESS: {
      return { ...state, loading: false, errors: false };
    }

    case DatasourcesTypes.UPDATE_DATASORCES_ERROR: {
      return { ...state, loading: false, errors: true };
    }
    case DatasourcesTypes.FETCH_SAMPLE_RECORDS: {
      return { ...state, loading: true, errors: false };
    }
    case DatasourcesTypes.FETCH_SAMPLE_RECORDS_SUCCESS: {
      return {
        ...state,
        loading: false,
        errors: false,
        sampleRecords: action.payload,
      };
    }
    default: {
      return state;
    }
  }
};
export { reducer as datasourceReducer };
