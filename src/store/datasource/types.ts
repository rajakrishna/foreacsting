export interface Datasources {
    data: [];
  }
  export enum DatasourcesTypes {
    FETCH_DATASORCES = "@@datasources/FETCH_DATASORCES",
    FETCH_DATASORCES_SUCCESS = "@@datasources/FETCH_DATASORCES_SUCCESS",
    FETCH_DATASORCES_ERROR = "@@datasources/FETCH_DATASORCES_ERROR",
    CREATE_DATASORCES = "@@datasources/CREATE_DATASORCES",
    CREATE_DATASORCES_SUCCESS = "@@datasources/CREATE_DATASORCES_SUCCESS",
    CREATE_DATASORCES_ERROR = "@@datasources/CREATE_DATASORCES_ERROR",
    DELETE_DATASORCES = "@@datasources/DELETE_DATASORCES",
    DELETE_DATASORCES_SUCCESS = "@@datasources/DELETE_DATASORCES_SUCCESS",
    DELETE_DATASORCES_ERROR = "@@datasources/DELETE_DATASORCES_ERROR",
    UPDATE_DATASORCES = "@@datasources/UPDATE_DATASORCES",
    UPDATE_DATASORCES_SUCCESS = "@@datasources/UPDATE_DATASORCES_SUCCESS",
    UPDATE_DATASORCES_ERROR = "@@datasources/UPDATE_DATASORCES_ERROR",
    FETCH_SAMPLE_RECORDS = "@@datasources/FETCH_SAMPLE_RECORDS",
    FETCH_SAMPLE_RECORDS_SUCCESS = "@@datasources/FETCH_SAMPLE_RECORDS_SUCCESS",
    FETCH_SAMPLE_RECORDS_ERROR = "@@datasources/FETCH_SAMPLE_RECORDS_ERROR",
  }
  
  export interface datasourcesState {
    readonly loading: boolean;
    readonly data: Datasources;
    readonly errors?: boolean;
  }
  