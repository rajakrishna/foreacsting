import { FeedBackTypes } from "./types";

import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";

import { ApplicationState } from "../index";
import { saveFeedBack } from "../../api";

export type AppThunk = ThunkAction<
  void,
  ApplicationState,
  null,
  Action<string>
>;

export const submitFeedBack: AppThunk = (data:any) => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    saveFeedBack(data)
      .then((response: any) => {
        const data = response.data;

        return dispatch({
          type: FeedBackTypes.SUBMIT_FEEDBACK_SUCCESS,
          payload: data.reverse(),
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: FeedBackTypes.SUBMIT_FEEDBACK_ERROR,
        });
      });
  };
};
