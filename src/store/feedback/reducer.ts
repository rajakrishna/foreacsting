import { Reducer } from "redux";
import { feedBackState, FeedBackTypes } from "./types";

export const initialState: any = {
    feedback:{},
    errors: false,
    loading: false,
  };
  const reducer: Reducer<feedBackState> = (state = initialState, action) => {

    switch (action.type) {
      case FeedBackTypes.SUBMIT_FEEDBACK: {
        return { ...state, loading: true,errors:false };
      }
      case FeedBackTypes.SUBMIT_FEEDBACK_SUCCESS: {
        return { ...state, loading: false, errors: false };
      }
     
      case FeedBackTypes.SUBMIT_FEEDBACK_ERROR: {
        return { ...state, loading: false, errors:true};
      }
      default: {
        return state;
      }
    }
  };
  export { reducer as feedBackReducer };
