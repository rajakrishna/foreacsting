export interface Feedback {
  data: {};
}
export enum FeedBackTypes {
  SUBMIT_FEEDBACK = "@@feedback/SUBMIT_FEEDBACK",
  SUBMIT_FEEDBACK_SUCCESS = "@@feedback/SUBMIT_FEEDBACK_SUCCESS",
  SUBMIT_FEEDBACK_ERROR = "@@feedback/SUBMIT_FEEDBACK_ERROR",
}

export interface feedBackState {
  readonly loading: boolean;
  readonly data: Feedback;
  readonly errors?: boolean;
}
