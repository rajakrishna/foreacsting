import { ForeCastingActionTypes } from "./types";

import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";

import { ApplicationState } from "../index";
import {
  CreateDataset,
  CreateDataSource,
  CreateModel,
  getAllModels,
  getDatasetSampleRecords,
  getWorkflowDetails,
  UploadDataSet,
} from "../../api";

export type AppThunk = ThunkAction<
  void,
  ApplicationState,
  null,
  Action<string>
>;

export const fetchForeCastingRuns: AppThunk = (id:any) => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    getAllModels(id)
      .then((response: any) => {
        const data = response.data;

        return dispatch({
          type: ForeCastingActionTypes.FETCH_FORECASTING_RUNS_SUCCESS,
          payload: data.reverse(),
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: ForeCastingActionTypes.FETCH_FORECASTING_RUNS_ERROR,
        });
      });
  };
};
type ThunkResult<R> = ThunkAction<R, any, any, any>;

export function uploadDataset(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await UploadDataSet(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: ForeCastingActionTypes.UPLOAD_DATASET_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: ForeCastingActionTypes.UPLOAD_DATASET_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export function createDataset(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await CreateDataset(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: ForeCastingActionTypes.UPLOAD_DATASET_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: ForeCastingActionTypes.UPLOAD_DATASET_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export function createDatasource(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await CreateDataSource(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: ForeCastingActionTypes.UPLOAD_DATASET_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        
        dispatch({
          type: ForeCastingActionTypes.UPLOAD_DATASET_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}

export function getSampleRecords(id: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await getDatasetSampleRecords(id)
      .then((res: any) => {
        const data = res.data;
        let keys: any[] = [];
        keys = data[0];
        resposeData = data;
       
        dispatch({
          type: ForeCastingActionTypes.FETCH_SAMPLE_RECORDS_SUCCESS,
          payload: keys,
        });
        dispatch({
          type: ForeCastingActionTypes.SET_SAMPLE_RECORDS,
          payload: data,
        });
       
      })
      .catch((error: any) => {
        return dispatch({
          type: ForeCastingActionTypes.FETCH_FORECASTING_RUNS_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}

// export const getSampleRecords1: AppThunk = (id: any) => {
//   let resposeData: null = null;

//   return async (dispatch: Dispatch, state: ApplicationState): any => {
//     await getDatasetSampleRecords(id)
//       .then((res: any) => {
//         const data = res.data;
//         let keys: any[] = [];
//         keys = data[0];
//         resposeData = data;
       
//         dispatch({
//           type: ForeCastingActionTypes.FETCH_SAMPLE_RECORDS_SUCCESS,
//           payload: keys,
//         });
//         dispatch({
//           type: ForeCastingActionTypes.SET_SAMPLE_RECORDS,
//           payload: data,
//         });
       
//       })
//       .catch((error: any) => {
//         return dispatch({
//           type: ForeCastingActionTypes.FETCH_FORECASTING_RUNS_ERROR,
//         });
//       });
//       return Promise.resolve(resposeData);

//   };
// };
export function createModel(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await CreateModel(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: ForeCastingActionTypes.CREATE_MODEL_SUCCESS,
          payload: data.ref_id,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: ForeCastingActionTypes.CREATE_MODEL_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export function getWorkflow(name: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await getWorkflowDetails(name)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
       
      })
      .catch((error: any) => {
       
      });
    return Promise.resolve(resposeData);
  };
}
export const setVersionId: AppThunk = (id:any) => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    return dispatch({
      type: ForeCastingActionTypes.SET_VERSION_ID,
      payload: id,
    });
  };
};
