import { Reducer } from "redux";

import { ForeCastingActionTypes, foreCastingState } from "./types";
export const initialState: any = {
  runs: [],
  errors: undefined,
  loading: false,
  selectedmodel:{},
  keys:[],
  skuIds:[],
  refId:'',
  sampleRecords:[],
  fileUploadError:false,
  submitModelError:false,
  versionId:''
};

const reducer: Reducer<foreCastingState> = (state = initialState, action) => {

  switch (action.type) {
    case ForeCastingActionTypes.FETCH_FORECASTING_RUNS: {
      return { ...state, loading: true };
    }
    case ForeCastingActionTypes.FETCH_FORECASTING_RUNS_SUCCESS: {
      return { ...state, loading: false, runs: action.payload,versionId: '' };
    }
    case ForeCastingActionTypes.SET_SAMPLE_RECORDS: {
      return { ...state, loading: false, sampleRecords: action.payload };
    }
    case ForeCastingActionTypes.SET_SKU_IDS: {
      return { ...state, loading: false, skuIds: action.payload };
    }
    case ForeCastingActionTypes.FETCH_FORECASTING_RUNS_ERROR: {
      return { ...state, loading: false, errors: action.payload };
    }
    case ForeCastingActionTypes.FETCH_SAMPLE_RECORDS: {
      return { ...state, loading: true };
    }
    case ForeCastingActionTypes.FETCH_SAMPLE_RECORDS_SUCCESS: {
      return { ...state, loading: false, keys: action.payload };
    }
    case ForeCastingActionTypes.FETCH_SAMPLE_RECORDS_ERROR: {
      return { ...state, loading: false, errors: action.payload };
    }
    case ForeCastingActionTypes.SELECTED_FORECASTING_RUN: {
      return { ...state, loading: false, selectedmodel: action.payload };
    }
    case ForeCastingActionTypes.CREATE_MODEL_SUCCESS: {
      return { ...state, loading: false, refId: action.payload };
    }
    case ForeCastingActionTypes.UPLOAD_DATASET_ERROR: {
      return { ...state, fileUploadError: true };
    }
    case ForeCastingActionTypes.UPLOAD_DATASET_SUCCESS: {
      return { ...state, fileUploadError: false };
    }
    case ForeCastingActionTypes.CREATE_MODEL_ERROR: {
      return { ...state, submitModelError: true };
    }
    case ForeCastingActionTypes.SET_VERSION_ID: {
      return { ...state, versionId: action.payload };
    }

    default: {
      return state;
    }
  }
};

export { reducer as foreCastingReducer };
