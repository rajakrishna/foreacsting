export interface ForeCasting {
  data:[]
}
export enum ForeCastingActionTypes {
  FETCH_FORECASTING_RUNS = "@@forecasting/FETCH_FORECASTING_RUNS",
  FETCH_FORECASTING_RUNS_SUCCESS = "@@forecasting/FETCH_FORECASTING_RUNS_SUCCESS",
  FETCH_FORECASTING_RUNS_ERROR = "@@forecasting/FETCH_FORECASTING_RUNS_ERROR",
  SELECTED_FORECASTING_RUN = "@@forecasting/SELECTED_FORECASTING_RUN",
  UPLOAD_DATASET = "@@forecasting/UPLOAD_DATASET",
  UPLOAD_DATASET_SUCCESS = "@@forecasting/UPLOAD_DATASET_SUCCESS",
  UPLOAD_DATASET_ERROR = "@@forecasting/UPLOAD_DATASET_ERROR",
  FETCH_SAMPLE_RECORDS = "@@forecasting/FETCH_SAMPLE_RECORDS",
  FETCH_SAMPLE_RECORDS_SUCCESS = "@@forecasting/FETCH_SAMPLE_RECORDS_SUCCESS",
  SET_SKU_IDS = "@@forecasting/SET_SKU_IDS",
  FETCH_SAMPLE_RECORDS_ERROR = "@@forecasting/FETCH_SAMPLE_RECORDS_ERROR",
  SET_SAMPLE_RECORDS = "@@forecasting/SET_SAMPLE_RECORDS",
  CREATE_MODEL = "@@forecasting/CREATE_MODEL",
  CREATE_MODEL_SUCCESS = "@@forecasting/CREATE_MODEL_SUCCESS",
  CREATE_MODEL_ERROR = "@@forecasting/CREATE_MODEL_ERROR",
  SET_VERSION_ID = "@@forecasting/SET_VERSION_ID",

}

export interface foreCastingState {
  readonly loading: boolean;
  readonly data: ForeCasting;
  readonly errors?: string;
}
