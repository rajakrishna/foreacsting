import { combineReducers } from "redux";
import { connectRouter } from "connected-react-router";

import { History } from "history";

import { RouterState } from "connected-react-router";
import { foreCastingState } from "./foreCasting/types";
import { foreCastingReducer } from "./foreCasting/reducer";
import { authReducer } from "./auth/reducer";
import { notificationsState } from "./notifications/types";
import { notificationsReducer } from "./notifications/reducer";
import { feedBackState } from "./feedback/types";
import { feedBackReducer } from "./feedback/reducer";
import { datasourceReducer } from "./datasource/reducer";
import { datasourcesState } from "./datasource/types";
import { projectsState } from "./projects/types";
import { projectsReducer } from "./projects/reducer";
import { BigCommerceState } from "./bigcommerce/types";
import { BigCommerceReducer } from "./bigcommerce/reducer";

export interface ApplicationState {
  bigcommerce:BigCommerceState;
  projects:projectsState;
  datasource:datasourcesState;
  forecasting: foreCastingState;
  notifications:notificationsState;
  feedback:feedBackState;
  auth: any;
  router: RouterState;
  
}

export const createRootReducer = (history: History) =>
  combineReducers({
    bigcommerce:BigCommerceReducer,
    projects:projectsReducer,
    datasource:datasourceReducer,
    foreCasting: foreCastingReducer,
    notifications:notificationsReducer,
    feedback:feedBackReducer,
    auth: authReducer,
    router: connectRouter(history),
  });
