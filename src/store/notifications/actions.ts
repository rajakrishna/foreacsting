import { NotificationsTypes } from "./types";

import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";

import { ApplicationState } from "../index";
import { getAllNotifications } from "../../api";

export type AppThunk = ThunkAction<
  void,
  ApplicationState,
  null,
  Action<string>
>;

export const getNotifications: AppThunk = () => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    getAllNotifications()
      .then((response: any) => {
        const data = response.data;

        return dispatch({
          type: NotificationsTypes.FETCH_NOTIFICATIONS_SUCCESS,
          payload: data.reverse(),
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: NotificationsTypes.FETCH_NOTIFICATIONS_ERROR,
        });
      });
  };
};
