import { Reducer } from "redux";
import { notificationsState, NotificationsTypes } from "./types";

export const initialState: any = {
    notifications: [],
    errors: undefined,
    loading: false,
   
  };
  const reducer: Reducer<notificationsState> = (state = initialState, action) => {

    switch (action.type) {
      case NotificationsTypes.FETCH_NOTIFICATIONS: {
        return { ...state, loading: true };
      }
      case NotificationsTypes.FETCH_NOTIFICATIONS_SUCCESS: {
        return { ...state, loading: false, notifications: action.payload };
      }
     
      case NotificationsTypes.FETCH_NOTIFICATIONS_ERROR: {
        return { ...state, loading: false, notifications:[]};
      }
      default: {
        return state;
      }
    }
  };
  export { reducer as notificationsReducer };
