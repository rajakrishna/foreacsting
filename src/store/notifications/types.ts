export interface Notifications {
  data: [];
}
export enum NotificationsTypes {
  FETCH_NOTIFICATIONS = "@@notifications/FETCH_NOTIFICATIONS",
  FETCH_NOTIFICATIONS_SUCCESS = "@@notifications/FETCH_NOTIFICATIONS_SUCCESS",
  FETCH_NOTIFICATIONS_ERROR = "@@notifications/FETCH_NOTIFICATIONS_ERROR",
}

export interface notificationsState {
  readonly loading: boolean;
  readonly data: Notifications;
  readonly errors?: string;
}
