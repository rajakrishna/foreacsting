import { Action, Dispatch } from "redux";
import { ThunkAction } from "redux-thunk";

import { ApplicationState } from "../index";
import {
  createProject,
  deleteProject,
  getAllUsers,
  getProjects,
  updateProject,
} from "../../api";
import { ProjectsTypes } from "./types";

export type AppThunk = ThunkAction<
  void,
  ApplicationState,
  null,
  Action<string>
>;

type ThunkResult<R> = ThunkAction<R, any, any, any>;

export const fetchProjects: AppThunk = () => {
  let resposeData: null = null;

  return async (dispatch: Dispatch, state: ApplicationState): Promise<any> => {
    await getProjects()
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        return dispatch({
          type: ProjectsTypes.FETCH_PROJECTS_SUCCESS,
          payload: data.reverse(),
        });
      })
      .catch((error: any) => {
        console.log("error project");
        return dispatch({
          type: ProjectsTypes.FETCH_PROJECTS_ERROR,
        });
      });
      return Promise.resolve(resposeData);

  };
  

};
export const fetchUsers: AppThunk = () => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    getAllUsers()
      .then((response: any) => {
        const data = response.data;

        return dispatch({
          type: ProjectsTypes.FETCH_USERS_SUCCESS,
          payload: data.reverse(),
        });
      })
      .catch((error: any) => {
        return dispatch({
          type: ProjectsTypes.FETCH_USERS_ERROR,
        });
      });
  };
};
export function projectCreatet(data: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await createProject(data)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: ProjectsTypes.CREATE_PROJECTS_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: ProjectsTypes.CREATE_PROJECTS_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export function projectUpdate(data: any, id: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await updateProject(data, id)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: ProjectsTypes.UPDATE_PROJECTS_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: ProjectsTypes.UPDATE_PROJECTS_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export function projectDelete(id: any): ThunkResult<Promise<any>> {
  let resposeData: null = null;
  return async (dispatch, getState) => {
    await deleteProject(id)
      .then((response: any) => {
        const data = response.data;
        resposeData = data;
        dispatch({
          type: ProjectsTypes.DELETE_PROJECTS_SUCCESS,
          payload: data,
        });
      })
      .catch((error: any) => {
        dispatch({
          type: ProjectsTypes.DELETE_PROJECTS_ERROR,
        });
      });
    return Promise.resolve(resposeData);
  };
}
export const setProjectId: AppThunk = (id: any) => {
  return (dispatch: Dispatch, state: ApplicationState): any => {
    return dispatch({
      type: ProjectsTypes.SET_PROJECT,
      payload: id,
    });
  };
};
