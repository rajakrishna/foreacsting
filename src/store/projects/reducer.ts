import { Reducer } from "redux";
import { projectsState, ProjectsTypes } from "./types";

export const initialState: any = {
    projects: [],
    errors: undefined,
    loading: false,
    project_id:"",
    users:[]

};
const reducer: Reducer<projectsState> = (state = initialState, action) => {

    switch (action.type) {
        case ProjectsTypes.FETCH_PROJECTS: {
            return { ...state, loading: true };
        }
        case ProjectsTypes.FETCH_PROJECTS_SUCCESS: {
            return { ...state, loading: false, projects: action.payload,project_id:action.payload[0].ref_id };
        }

        case ProjectsTypes.FETCH_PROJECTS_ERROR: {
            return { ...state, loading: false, projects: [] };
        }
        case ProjectsTypes.CREATE_PROJECTS_SUCCESS: {
            return { ...state, loading: false };
        }
        case ProjectsTypes.FETCH_USERS: {
            return { ...state, loading: true };
        }
        case ProjectsTypes.FETCH_USERS_SUCCESS: {
            return { ...state, loading: false, users: action.payload };
        }

        case ProjectsTypes.FETCH_USERS_ERROR: {
            return { ...state, loading: false, users: [] };
        }
        case ProjectsTypes.CREATE_PROJECTS_SUCCESS: {
            return { ...state, loading: false };
        }

        case ProjectsTypes.CREATE_PROJECTS_ERROR: {
            return { ...state, loading: false };
        }
        case ProjectsTypes.UPDATE_PROJECTS_SUCCESS: {
            return { ...state, loading: false };
        }

        case ProjectsTypes.UPDATE_PROJECTS_ERROR: {
            return { ...state, loading: false };
        }
        case ProjectsTypes.DELETE_PROJECTS_SUCCESS: {
            return { ...state, loading: false };
        }

        case ProjectsTypes.DELETE_PROJECTS_ERROR: {
            return { ...state, loading: false };
        }
        case ProjectsTypes.SET_PROJECT: {
            return { ...state, loading: false, project_id: action.payload };
        }
        default: {
            return state;
        }
    }
};
export { reducer as projectsReducer };
