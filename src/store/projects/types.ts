export interface Projects {
    data: [];
}
export enum ProjectsTypes {
    FETCH_PROJECTS = "@@projects/FETCH_PROJECTS",
    FETCH_PROJECTS_SUCCESS = "@@projects/FETCH_PROJECTS_SUCCESS",
    FETCH_PROJECTS_ERROR = "@@projects/FETCH_PROJECTS_ERROR",
    CREATE_PROJECTS = "@@projects/CREATE_PROJECTS",
    CREATE_PROJECTS_SUCCESS = "@@projects/CREATE_PROJECTS_SUCCESS",
    CREATE_PROJECTS_ERROR = "@@projects/CREATE_PROJECTS_ERROR",
    UPDATE_PROJECTS = "@@projects/UPDATE_PROJECTS",
    UPDATE_PROJECTS_SUCCESS = "@@projects/UPDATE_PROJECTS_SUCCESS",
    UPDATE_PROJECTS_ERROR = "@@projects/UPDATE_PROJECTS_ERROR",
    DELETE_PROJECTS = "@@projects/DELETE_PROJECTS",
    DELETE_PROJECTS_SUCCESS = "@@projects/DELETE_PROJECTS_SUCCESS",
    DELETE_PROJECTS_ERROR = "@@projects/DELETE_PROJECTS_ERROR",
    FETCH_USERS = "@@projects/FETCH_USERS",
    FETCH_USERS_SUCCESS = "@@projects/FETCH_USERS_SUCCESS",
    FETCH_USERS_ERROR = "@@projects/FETCH_USERS_ERROR",
    SET_PROJECT = "@@projects/SET_PROJECT",

}

export interface projectsState {
    readonly loading: boolean;
    readonly data: Projects;
    readonly errors?: string;
}
