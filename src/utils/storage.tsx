export function loadToken() {
    try {
      const token = sessionStorage.getItem('token');
      if(token === null) {
        return undefined;
      }
      return token;
    } catch (err) {
      console.info('Session storage cannot be accessed!',err)
      return undefined;
    }
  }